/*
 * 
 */
package queryengine;


import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.TreeSet;

// TODO: Auto-generated Javadoc
/**
 * The Class QueryEngine.
 */
public class QueryEngine{
	
	public String[] getYears(ResultSet rs){
		String[] results;
		
		TreeSet<String> years = new TreeSet<>();
		try {
			rs.beforeFirst();
			while(rs.next()){
				years.add(rs.getString("YEAR"));
			}
		} catch (SQLException e1) {
			e1.printStackTrace();
		}
		results = new String[years.size()];
		int count=0;
		for(String year : years){
			results[count] =year;
			count++;
		}
		return results;
	}
	
}
