/*
 * 
 */
package frontend;
/***
 * 
 * @author amogh
 * @purpose : This class is used to represent an Inproceedings record from dblp.xml
 * @module : parsing-engine : backend
 *
 */

//import statements 
import java.util.ArrayList;
import java.util.List;

// TODO: Auto-generated Javadoc
/**
 * The Class InProceedings.
 */
public class InProceedings implements Publication{

	/** The pkey variable holds primary key. */
	//attributes of InProceedings class
	private String pKey;
	
	/** stores the author. */
	private ArrayList<String> author;
	
	/** stores the year. */
	private int year;
	
	/** stores the month. */
	private String month;
	
	/** stores the title. */
	private String title;
	
	/** stores the book title. */
	private String bookTitle;

	//constructors

	/**
	 * Instantiates a new in proceedings.
	 */
	public InProceedings() {
		pKey="";
		author = new ArrayList<>();
		year = 0;
		month = "";
		title = "";
		bookTitle = "";
	}
	
	/**
	 * Instantiates a new in proceedings.
	 *
	 * @param key the key
	 */
	public InProceedings(String key) {
		this.setKey(key);
		author = new ArrayList<>();
		year = 0;
		month = "";
		title = "";
		bookTitle = "";
	}

	//getters and setters methods for each of the attributes

	/**
	 * Sets the author.
	 *
	 * @param name the new author
	 */
	// function to set author attribute
	public void setAuthor(String name){
		author.add(name);
	}

	/**
	 * Gets the author.
	 *
	 * @return the author
	 */
	//function to get list of authors for an Inproceedings object
	public List<String> getAuthor(){
		return author;
	}

	/**
	 * Sets the year.
	 *
	 * @param year the new year
	 */
	//function to set the year attribute
	public void setYear(int year){
		this.year = year;
	}

	/**
	 * Gets the year.
	 *
	 * @return the year
	 */
	//function to return year of the Inproceedings
	public int getYear(){
		return year;
	}

	/**
	 * Sets the month.
	 *
	 * @param month the new month
	 */
	//function to set the month attribute
	public void setMonth(String month){
		this.month = month;
	}

	/**
	 * Gets the month.
	 *
	 * @return the month
	 */
	//function to get the month attribute of InProceedings
	public String getMonth(){
		return month;
	}

	/**
	 * Sets the title.
	 *
	 * @param title the new title
	 */
	//function to set title of inProceedings
	public void setTitle(String title){
		this.title = title;
	}

	/**
	 * Gets the title.
	 *
	 * @return the title
	 */
	//function to get title of inproceedings
	public String getTitle(){
		return this.title;
	}

	/**
	 * Sets the book title.
	 *
	 * @param title the new book title
	 */
	//function to set book title
	public void setBookTitle(String title){
		this.bookTitle = title;
	}

	/**
	 * Gets the book title.
	 *
	 * @return the book title
	 */
	//function to get book title
	public String getBookTitle(){
		return this.bookTitle;
	}


	/**
	 * Sets the key.
	 *
	 * @param key the new key
	 */
	//function to set key  
	public void setKey(String key){
		this.pKey = key;
	}

	/**
	 * Gets the key.
	 *
	 * @return the key
	 */
	//function to get book title
	public String getKey(){
		return this.pKey;
	}


	/* (non-Javadoc)
	 * @see frontend.Publication#process(java.lang.String, java.lang.String)
	 */
	public void process(String qName,String value){  //NOSONAR
		// If the closing tag is inproceedings , we load the records to the
		// database
		if (commons.Constants.INPROCEEDINGS.equalsIgnoreCase(qName.trim())) {
			this.loadToDB();

		}
		// if the closing tag is author , we add the author to
		// the author attribute of Inproceedings object
		if (commons.Constants.AUTHOR.equalsIgnoreCase(qName.trim())) {
			this.setAuthor(value);

		}
		// if the closing Tag is title, we set the title of
		// inproceedings object with the value in valueText
		if (commons.Constants.TITLE.equalsIgnoreCase(qName.trim())) {
			this.setTitle(value);

		}
		// if the closing tag is month, then the month attribute
		// of inproceedings object is set
		if (commons.Constants.MONTH.equalsIgnoreCase(qName.trim())) {
			this.setMonth(value);

		}
		// if the closing tag is year, then the year attribute
		// of inproceedings object is set
		if (commons.Constants.YEAR.equalsIgnoreCase(qName.trim())) {
			this.setYear(Integer.parseInt(value));

		}

		// if the closing tag is book title, then the book title
		// attribute of inproceedings object is set
		if (commons.Constants.BOOK_TITLE.equalsIgnoreCase(qName.trim())) {
			this.setBookTitle(value);
		}

	}
	
	/**
	 * Load to DB.
	 */
	public void loadToDB(){
		DBUtils.getInstance().addInproceedingsToDB(this);
	}

}
