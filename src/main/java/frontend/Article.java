/*
 * 
 */
package frontend; 

/***
 * 
 * @author amogh

 * @purpose : This class is used to represent an article record from dblp.xml
 * @module : parsing-engine : backend
 *
 */

//import statements
import java.util.ArrayList;
import java.util.List;

// TODO: Auto-generated Javadoc
/**
 * The Class Article.
 */
public class Article implements Publication{

	/** The article key. */
	// attributes of class
	private String articleKey;
	
	/** The author. */
	private ArrayList<String> author;
	
	/** The year. */
	private int year;
	
	/** The month. */
	private String month;
	
	/** The title. */
	private String title;
	
	/** The journal. */
	private String journal;
	
	/** The volume. */
	private String volume;

	/**
	 * Instantiates a new article.
	 */
	// default constructors
	public Article() {
		articleKey = "";
		author = new ArrayList<>();
		year = 0;
		month = "";
		title = "";
		journal = "";
		volume = "";
	}
	
	/**
	 * Instantiates a new article.
	 *
	 * @param key the key
	 */
	public Article(String key) {
		this.setKey(key);
		author = new ArrayList<>();
		year = 0;
		month = "";
		title = "";
		journal = "";
		volume = "";
	}

	// getter and setter methods
	
	/**
	 * Sets the key.
	 *
	 * @param articleKey the new key
	 */
	// function to set articleKey attribute
	public void setKey(String articleKey) {
		this.articleKey = articleKey;
	}

	/**
	 * Gets the key.
	 *
	 * @return the key
	 */
	// function to get articleKey for an article object
	public String getKey() {
		return this.articleKey;
	}

	/**
	 * Sets the author.
	 *
	 * @param name the new author
	 */
	// function to set author attribute
	public void setAuthor(String name) {
		author.add(name);
	}

	/**
	 * Gets the author.
	 *
	 * @return the author
	 */
	// function to get list of authors for an article object
	public List<String> getAuthor() {
		return author;
	}

	/**
	 * Sets the year.
	 *
	 * @param year the new year
	 */
	// function to set the year attrubite of article
	public void setYear(int year) {
		this.year = year;
	}

	/**
	 * Gets the year.
	 *
	 * @return the year
	 */
	// function to return year of article
	public int getYear() {
		return year;
	}

	/**
	 * Sets the month.
	 *
	 * @param month the new month
	 */
	// function to set the month attribute of article
	public void setMonth(String month) {
		this.month = month;
	}

	/**
	 * Gets the month.
	 *
	 * @return the month
	 */
	// function to get the month attribute of article
	public String getMonth() {
		return month;
	}

	/**
	 * Sets the title.
	 *
	 * @param title the new title
	 */
	// function to set title of article
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * Gets the title.
	 *
	 * @return the title
	 */
	// function to get title of article
	public String getTitle() {
		return this.title;
	}

	/**
	 * Sets the journal.
	 *
	 * @param name the new journal
	 */
	// funtion to set journal name of article
	public void setJournal(String name) {
		this.journal = name;
	}

	/**
	 * Gets the journal.
	 *
	 * @return the journal
	 */
	// function to get journal name of article
	public String getJournal() {
		return this.journal;
	}

	/**
	 * Sets the volume.
	 *
	 * @param volume the new volume
	 */
	// function to set the volume of article
	public void setVolume(String volume) {
		this.volume = volume;
	}

	/**
	 * Gets the volume.
	 *
	 * @return the volume
	 */
	// function to get the volume of article
	public String getVolume() {
		return this.volume;
	}
	
	/* (non-Javadoc)
	 * @see frontend.Publication#process(java.lang.String, java.lang.String)
	 */
	public void process(String qName,String value){ //NOSONAR
		// if the closing tag is article load it to database
		if (commons.Constants.ARTICLE.equalsIgnoreCase(qName.trim())) {
			loadToDB();
		}
		// if the closing tag is author , we add the author to
		// the author attribute of article object
		if (commons.Constants.AUTHOR.equalsIgnoreCase(qName.trim())) {
			this.setAuthor(value);
		}
		// if the closing Tag is title, we set the title of
		// article object with the value in valueText
		if (commons.Constants.TITLE.equalsIgnoreCase(qName.trim())) {
			this.setTitle(value);
		}
		// if the closing tag is month, then the month attribute
		// of article object is set
		if (commons.Constants.MONTH.equalsIgnoreCase(qName.trim())) {
			this.setMonth(value);
		}
		// if the closing tag is year, then the year attribute
		// of article object is set
		if (commons.Constants.YEAR.equalsIgnoreCase(qName.trim())) {
			this.setYear(Integer.parseInt(value.trim()));
		}

		// if the closing tag is journal, then the journal
		// attribute of article object is set
		if (commons.Constants.JOURNAL.equalsIgnoreCase(qName.trim())) {
			this.setJournal(value);
		}

		// if the closing tag is journal, then the journal
		// attribute of article object is set
		if (commons.Constants.VOLUME.equalsIgnoreCase(qName.trim())) {
			this.setVolume(value);
		}
	}
	
	/**
	 * Load to DB.
	 */
	public void loadToDB(){
		DBUtils.getInstance().addArticleToDB(this);
	}
}
