/*
 * 
 */
package frontend;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;
// TODO: Auto-generated Javadoc
//import org.apache.log4j.*;

/**
 * The Class Handler.
 */
public class Handler extends DefaultHandler {

	/** The value text. */
	StringBuilder valueText; // this variable represents the content
	// of the tag

	// declaring a logger variable
	// private static final Logger logger = Logger.getLogger(Handler.class);

	/**
	 * variable to store global line count to execute batches at periodic time
	 * intrevals.
	 */
	private long globalLineCount = 0;
	
	/**
	 * Function to set globalCount variable
	 * 
	 */
	public void setGlobalCount(int x){
		this.globalLineCount=x;
	}

	/** variable to store The lines processed. */
	private long linesProcessed = 0;

	/** The publication. */
	Publication publication;

	/**
	 * Called when document begins
	 */
	@Override
	public void startDocument() {
		// obtaining a db instance when file opens. This instance is retained
		// throughout the file
		DBUtils.getInstance();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.xml.sax.helpers.DefaultHandler#startElement(java.lang.String,
	 * java.lang.String, java.lang.String, org.xml.sax.Attributes)
	 */
	// This function is called when a opening tag is encountered
	@Override
	public void startElement(String uri, String localname, String qname, Attributes attributes) throws SAXException {

		
		// incrementing global count when we encounter an opening tag
					globalLineCount++;
					// lines processed stores total lines processed in documents
					linesProcessed++;
					if (globalLineCount >= 100000) {
						// executing batch when we get 1000000 entries
						DBUtils.getInstance().executeBatch();
						System.out.println("Number of lines processed " + linesProcessed);
						globalLineCount = 0;
					}
		// setting value of inproceedingsFlag to true if we find a
		// inproceedings opening tag

		if (qname.equals("inproceedings") || qname.equals("www") || qname.equals("article")) {

			// getting appropriate object using publication factory
			PublicationFactory factory = new PublicationFactory();
			publication = factory.getPublication(qname, attributes, publication);

			// using this variable to store any characters we encounter between
			// opening and closing tags
			valueText = new StringBuilder();

		} 

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.xml.sax.helpers.DefaultHandler#endElement(java.lang.String,
	 * java.lang.String, java.lang.String)
	 */
	// function is called when the closing tag is encountered
	@Override
	public void endElement(String uri, String localName, String qName) throws SAXException {

		if (publication != null) {
			publication.process(qName, valueText.toString());
			valueText = new StringBuilder();
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.xml.sax.helpers.DefaultHandler#characters(char[], int, int)
	 */
	// function called when we encounter charcters between opening and closing
	// tag
	@Override
	public void characters(char[] ch, int start, int length) throws SAXException {

		if (valueText != null) {
			valueText.append(ch, start, length);
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.xml.sax.helpers.DefaultHandler#endDocument()
	 */
   //function encountered when we reach end of document. 
	@Override
	public void endDocument() throws SAXException {
		//executing remaining elements in batch as we have reached end of document
		DBUtils.getInstance().executeBatch();
	}

}
