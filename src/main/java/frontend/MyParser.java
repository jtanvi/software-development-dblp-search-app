/*
 * 
 */
package frontend;

import java.io.BufferedReader;
//import statements
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;
import javax.xml.parsers.ParserConfigurationException;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

// TODO: Auto-generated Javadoc
/**
 * * .
 *
 * @author: Amogh Huilgol
 * @lastModified : 3/18/2017
 * @purpose : This module is used to parse dblp.xml file and store records in MYSQL database
 * @module : parsing-engine : backend
 */


//Parser class definition
public class MyParser {

	
	// logger object to generate logs
	private static final Logger logger = Logger.getLogger(MyParser.class);


	//variable to store path of dblp.xml file
	private static String dblpFilename;

	//variable to store path of committee data

	/** The committee filename. */
	private static String committeeFilename ;
	
	/** The prop used to store details from config file */
	static Properties prop = new Properties();
	
	/** The inputstream to load config file */
	static InputStream input = null;


	/**
	 * Instantiates a new parser.
	 *
	 * @param dblpPath the dblp path
	 * @param committeePath the committee path
	 */
	public MyParser(String dblpPath, String committeePath){ 
		dblpFilename = dblpPath;
		committeeFilename = committeePath;	
	}

	/**
	 * parses data from xml and creates objects based on tags it encounters
	 *
	 * @return true, if successful
	 */
	public static  boolean readData() {

		// setting entity expansion limit to 10000000 from default 640000
		PropertyConfigurator.configure(commons.Constants.LOG4J_PROPERTIES); 
		System.setProperty("entityExpansionLimit", "10000000");

		// parser factory declaration
		SAXParserFactory parserFactory = SAXParserFactory.newInstance();


		try {
			input = new FileInputStream("config.properties");

			// load a properties file
			prop.load(input);
			committeeFilename = prop.getProperty("committeeLocation","committees");
			dblpFilename = prop.getProperty("dblpLocation", "dblp.xml");

			final File folder = new File(committeeFilename);
			CommitteeParser c = new CommitteeParser(); //NOSONAR
			
			//checking if folder exists. If the path name is wrong , then error is thrown 
			if(folder.exists()){
				//calling the function to load committee data. 
				c.parseAndLoad(folder);  //NOSONAR
			}else{
				logger.error("Did not find the folder or the folder is empty  "+ committeeFilename);
			}
            
			//executing batch on mysql to load remaining data
			DBUtils.getInstance().executeBatch();
			
			logger.info("Committee data parsed successfully");

			// defining a SAX parser
			SAXParser parser = parserFactory.newSAXParser();
			
			// handler to handle events while parsing xml file
			DefaultHandler handler = new Handler();

			parser.parse(dblpFilename, handler);

		} catch (SAXException | ParserConfigurationException | IOException e) {
			logger.error(e);
			logger.error(e.getMessage());
			return false;
		}
		return true;

	}

	/**
	 * Read authors.
	 *
	 * @return true, if successful
	 */
	public static boolean readAuthors(){
		
		//setting up log4j for error logging
		PropertyConfigurator.configure(commons.Constants.LOG4J_PROPERTIES); 
		//variable to record errors 
		boolean result =false;
		// variable to store path of author-info file
		String csvFile;
		// variable to store 
		BufferedReader br = null;
		String line = "";
		String cvsSplitBy = ",";
		int globalLineCount=0;
		int localLineCount=0;
		try {
            //reading config.properties file
			input = new FileInputStream("config.properties");

			// load a properties file
			prop.load(input);
			
			//getting path of author-info file
			csvFile = prop.getProperty("authorFile", "generated-author-info.csv");
			
			br = new BufferedReader(new FileReader(csvFile));
			while ((line = br.readLine()) != null) {
				
				//used to execute batch at periodic intervals
				globalLineCount++;
				//variable to keep track of number of lines executed
				localLineCount++;
				
				if(globalLineCount>1){
					// use comma as separator and process author-info file
					String[] fields = line.split(cvsSplitBy);
					if(fields !=null){
						if(fields.length>5){
							Author author = new Author();
							if(fields[0].contains("'")){
								String name = fields[0].replaceAll("'", "''");
								author.setName(name);
							}else{
								author.setName(fields[0]);
							}
							author.setUniversity(fields[1]);
							author.setAreaOfResearch(fields[2]);
							author.setPublicationsCount((int)Double.parseDouble(fields[3]));
							author.setLatestYearOfPublication((int)Double.parseDouble(fields[5]));
							DBUtils.getInstance().addAuthorToDB(author);
						}
					}
					//if number of lines processed is mor than 1000 we execute the batch.
					if (globalLineCount >= 1000) {
						DBUtils.getInstance().executeBatch();
						globalLineCount = 0;
					}
				}


			}

		} catch (IOException| NumberFormatException e) {
			//logging exceptions
			logger.error(e);
		} 
		finally {
			//closing connections and filestreams in case of an exception
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					logger.error(e);
				}
			}
		}
		DBUtils.getInstance().executeBatch();
		System.out.println(localLineCount);
		result = true;
		return result;
	}

	/**
	 * The main method.
	 *
	 * @param args the arguments
	 */
	/*public static void main(String[] args) { //NOSONAR
		PropertyConfigurator.configure(commons.Constants.LOG4J_PROPERTIES); //NOSONAR
		Boolean returnAuthorStatus = readAuthors();
		Boolean returnStatus = readData(); //NOSONAR
		
		if(returnAuthorStatus&&returnStatus){ //NOSONAR
			System.exit(0); //NOSONAR
		}else{//NOSONAR
			System.exit(1); //NOSONAR
		}//NOSONAR

	} //NOSONAR */


}