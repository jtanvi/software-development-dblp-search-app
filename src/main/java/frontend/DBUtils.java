package frontend;

import java.awt.Toolkit;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.Set;
import java.util.Vector;

import javax.swing.JDialog;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import com.mysql.jdbc.Connection;


// TODO: Auto-generated Javadoc
/**
 * The Class DBUtils. Implements singleton design pattern
 */
public class DBUtils {

	/** The db instance. */
	private static DBUtils dbInstance = new DBUtils();

	private static HashMap<String, Integer> authorPageNames;
	
	private static HashMap<String, Integer> authorConfNames;
	
	private static int maxCountAuthors=1 ;
	
	private static int maxCountConf=1 ;
	
	public static int getMaxCountConf() {
		return maxCountConf;
	}

	public int getMaxCountAuthors() {
		return maxCountAuthors;
	}

	/** The author names. */
	private static ArrayList<String> authorNames;

	/** The prop. */
	static Properties prop = new Properties();

	/** The input. */
	static InputStream input = null;

	/**
	 * Gets the author page names.
	 *
	 * @return the author page names
	 */
	public static HashMap<String, Integer> getAuthorPageNames() {
		return authorPageNames;
	}
	
	/**
	 * function to set prepared statement to null
	 * 
	 */
	public  void setStatement(String val){
		this.statement = null;
	}
	
	

	/**
	 * Sets the author page names.
	 *
	 * @param authorPageNames
	 *            : List of author page names
	 */
	public static void setAuthorPageNames(HashMap<String, Integer> authorPageNames) {
		DBUtils.authorPageNames = authorPageNames;
	}

	
	/**
	 * @return
	 */
	public static HashMap<String, Integer> getAuthorConfNames() {
		return authorConfNames;
	}

	/**
	 * @param authorConfNames
	 */
	public static void setAuthorConfNames(HashMap<String, Integer> authorConfNames) {
		DBUtils.authorConfNames = authorConfNames;
	}

	/**
	 * Gets the author names.
	 *
	 * @return the author names
	 */
	public ArrayList<String> getAuthorNames() {
		return authorNames;
	}

	/**
	 * Sets the author names.
	 *
	 * @param authorNames
	 *            the new author names
	 */
	public void setAuthorNames(ArrayList<String> authorNames) {
		this.authorNames = authorNames;
	}

	// declaring a connection variable to hold connection to database
	private Connection con;

	// statement used to execute sql in batches
	private PreparedStatement statement;
	
	

	// declaring a variable for logging
	private static final Logger logger = Logger.getLogger(DBUtils.class);

	/**
	 * Instantiates a new DB utils.
	 */
	private DBUtils() {
		// declaring a private constructor to satsify sonar linitng requirements
	}

	/**
	 * Gets the single instance of DBUtils.using singleton design pattern to
	 * maintain only a single copy of database
	 *
	 * @return single instance of DBUtils
	 */

	public static DBUtils getInstance() {
		if (dbInstance == null) {
			dbInstance = new DBUtils();
		}
		return dbInstance;
	}
	
	/**
	 * function to add query to statement
	 */
	public void addQuery(String q) throws Exception {
		if (statement == null) {
			statement = con.prepareStatement(q);
			
		}else{
			statement.addBatch(q);
		}
	}

	/**
	 * Close connection.
	 *
	 * @return the DB utils
	 */
	public DBUtils closeConnection() {
		dbInstance = null;
		return dbInstance;
	}

	/**
	 * Execute batch.
	 */
	// function to execute batch
	public void executeBatch() {
		try {
			if (statement != null) {
				int[] arr = statement.executeBatch();

			} else {
				logger.warn("Statement found to be null. terminating program");
			}

		} catch (SQLException e) {
			logger.error(e);
		}
	}

	/**
	 * * Function to get connection to mysql database.
	 *
	 * @return : Connection
	 */

	public Connection getConnection() {

		// setting up log4j configuration
		PropertyConfigurator.configure(commons.Constants.LOG4J_PROPERTIES);

		try {
			// if con is null , create a new connection , else return the
			// existing connection

			if (con == null) {
				try {
					input = new FileInputStream("config.properties");
					// load a properties file
					prop.load(input);
				} catch (IOException e) {
					logger.error("Unable to find config.properties file");
				}

				String driver = "com.mysql.jdbc.Driver";
				String url = prop.getProperty("jdbcString", "jdbc:mysql://localhost:3306/DBLP_SEARCH");

				String username = prop.getProperty("dbusername", "root");
				String password = prop.getProperty("dbpassword", "root");

				Class.forName(driver);

				// establishing a connection to mysql database
				con = (Connection) DriverManager.getConnection(url, username, password);
				return con;

			} else {
				return con;
			}

		} catch (Exception e) {
			logger.error(e);

		}

		return null;

	}
	
	
	
	/**
	 * Function to update year in committee by repalcing fse with 2014
	 * 
	 *
	 */
	
	public void updateYearInCommittee(){
		String text = "";
		try{
		con = getConnection();
		text= "update DBLP_SEARCH.COMMITTEE set COMMITTEE_YEAR='2014' where TRIM(COMMITTEE_YEAR) ='fse'";
		
		if (statement == null) {
			statement = con.prepareStatement(text);
		}else{
			statement.executeUpdate(text);
		}
		}catch(SQLException e){
			System.out.println(e);
		}
	}

	/**
	 * * Functions to process attributes of inproceedings.
	 *
	 * @param inpObject
	 *            the inp object
	 */

	// function to load inproceedings to database
	public void addInproceedingsToDB(InProceedings inpObject) {

		// setting up log4j configuration
		PropertyConfigurator.configure(commons.Constants.LOG4J_PROPERTIES);

		// initializing variable to hold query
		String sqlText = "";

		// stores list of queries to insert into author table
		ArrayList<String> authorSqlText = new ArrayList<>();
		StringBuilder authorText = new StringBuilder();

		try {
			// establishing a connection to mysql database
			con = getConnection();

			if (con != null) {
				// processing authors
				ArrayList<String> authors = (ArrayList<String>) inpObject.getAuthor();
				for (int i = 0; i < authors.size(); i++) {
					if (authorText == null || authorText.toString().equals("")) {
						authorText.append(authors.get(i).replaceAll("'", ""));
					} else {
						authorText.append("," + authors.get(i).replaceAll("'", ""));
					}

				}

				// generating query text
				sqlText = "insert  into DBLP_SEARCH.INPROCEEDINGS values('" + inpObject.getKey() + "','"
						+ inpObject.getTitle().replaceAll("'", "") + "'," + inpObject.getYear() + ",'"
						+ inpObject.getMonth() + "','" + inpObject.getBookTitle().replaceAll("'", "") + "','"
						+ authorText.toString() + "');";

				if (statement == null) {
					statement = con.prepareStatement(sqlText);
				}

				// adding insert statement to batch
				statement.addBatch(sqlText);

			}

		} catch (Exception e) {
			logger.error(e);

		}
	}

	/**
	 * Functions to process www object.
	 *
	 * @param wwwObject
	 *            the www object
	 */
	public void addWwwToDb(Www wwwObject) {

		// setting up configuration for log4j
		PropertyConfigurator.configure(commons.Constants.LOG4J_PROPERTIES);
		// initializing variables to hold sql statements
		ArrayList<String> authorSqlText = new ArrayList<>();
		String sqlText = "";
		try {
			// establishing connection to database
			con = getConnection();

			// processing authors and editors
			ArrayList<String> editors = (ArrayList<String>) wwwObject.getEditors();
			ArrayList<String> authors = (ArrayList<String>) wwwObject.getAuthors();
			StringBuilder authorText = new StringBuilder();
			StringBuilder editorText = new StringBuilder();
			for (int i = 0; i < authors.size(); i++) {
				// generating author queries

				if (authorText.toString().equals("")) {
					authorText.append(authors.get(i).replaceAll("'", ""));
				} else {
					authorText.append("," + authors.get(i).replaceAll("'", ""));
				}

			}

			for (int i = 0; i < editors.size(); i++) {
				// generating author queries

				if (editorText.toString().equals("")) {
					editorText.append(authors.get(i).replaceAll("'", ""));
				} else {
					editorText.append("," + authors.get(i).replaceAll("'", ""));
				}

			}

			if (con != null) {
				// generating sql statements
				sqlText = "insert  into DBLP_SEARCH.WWW(ID,WWW_YEAR,TITLE,AUTHOR_NAME) values('" + wwwObject.getKey()
						+ "'," + wwwObject.getYear() + ",'" + wwwObject.getTitle().replaceAll("'", "") + "','"
						+ authorText.toString() + "');";

				if (statement == null) {
					statement = con.prepareStatement(sqlText);
				}
				statement.addBatch(sqlText);
			}

		} catch (Exception e) {
			logger.error(e);
		}

	}

	/**
	 * *
	 * 
	 * Function to process attributes of articles.
	 *
	 * @param article
	 *            the article
	 */

	public void addArticleToDB(Article article) {

		// setting up configuration for log4j
		PropertyConfigurator.configure(commons.Constants.LOG4J_PROPERTIES);
		// initialization of variables for holding sql statements
		String sqlText = "";
		ArrayList<String> authorSqlText = new ArrayList<>();
		try {
			// establishing sql connection to database
			con = getConnection();
			StringBuilder authorText = new StringBuilder();

			// processing authors
			ArrayList<String> authors = (ArrayList<String>) article.getAuthor();

			for (int i = 0; i < authors.size(); i++) {

				if (authorText == null || authorText.toString().equals("")) {
					authorText.append(authors.get(i).replaceAll("'", ""));
				} else {
					authorText.append("," + authors.get(i).replaceAll("'", ""));
				}

			}

			if (con != null) {
				// generating sql statements
				sqlText = "insert  into DBLP_SEARCH.ARTICLE(ID,ARTICLE_YEAR,ARTICLE_MONTH,VOLUME,TITLE,JOURNAL,AUTHOR_NAME) values('"
						+ article.getKey() + "'," + article.getYear() + ",'" + article.getMonth() + "','"
						+ article.getVolume().replaceAll("'", "") + "','" + article.getTitle().replaceAll("'", "")
						+ "','" + article.getJournal().replaceAll("'", "") + "','" + authorText.toString() + "');";

				if (statement == null) {
					statement = con.prepareStatement(sqlText);
				}
				// adding sql statements to batch
				statement.addBatch(sqlText);

				// adding authors insert statements to batch
				for (int i = 0; i < authorSqlText.size(); i++) {
					statement.addBatch(authorSqlText.get(i));
				}

			}

		} catch (Exception e) {
			logger.error(e);

		}
	}

	/**
	 * *
	 * 
	 * Function to process attributes of committee.
	 *
	 * @param key
	 *            the key
	 * @param authorName
	 *            the author name
	 * @param chairFlag
	 *            the chair flag
	 * @param programChairFlag
	 *            the program chair flag
	 * @param conferenceFlag
	 *            the conference flag
	 * @param externalReviewFlag
	 *            the external review flag
	 * @param otherFlag
	 *            the other flag
	 */
	public void addCommitteeToDB(String key, String authorName, String chairFlag, String programChairFlag,
			String conferenceFlag, String externalReviewFlag, String otherFlag) {
		// setting up configuration of log4j
		PropertyConfigurator.configure(commons.Constants.LOG4J_PROPERTIES);
		// establishing sql connection to database
		con = getConnection();
		if (con != null) {

			try {
				// generating sql statement
				String sqlText = "INSERT  INTO COMMITTEE(ID,AUTHOR_NAME,GENERAL_CHAIR_FLAG,PROGRAM_CHAIR_FLAG,CONFERENCE_CHAIR_FLAG,EXTERNAL_REVIEW_FLAG,OTHER_FLAG,COMMITTEE_YEAR) VALUES('"
						+ key + "','" + authorName.replaceAll("'", "") + "','" + chairFlag + "','" + programChairFlag
						+ "','" + conferenceFlag + "','" + externalReviewFlag + "','" + otherFlag
						+ "',reverse(substring(reverse(substring(id,1,position('-'IN id)-1)),1,4)));";
				if (statement == null) {
					statement = con.prepareStatement(sqlText);
				}

				statement.addBatch(sqlText);
			} catch (SQLException e) {
				logger.error(e);
			}
		} else {
			logger.error("Connection object returned null at line 475 DBUtils.java");
		}

	}

	/**
	 * Adds the author to DB.
	 *
	 * @param author
	 *            the author
	 */
	public void addAuthorToDB(Author author) {

		// setting up configuration for log4j
		PropertyConfigurator.configure(commons.Constants.LOG4J_PROPERTIES);
		// initialization of variables for holding sql statements
		String sqlText = "";
		try {
			// establishing sql connection to database
			con = getConnection();

			if (con != null) {
				// generating sql statements
				sqlText = "insert  into DBLP_SEARCH.AUTHORS(AUTHOR_NAME,UNIVERSITY,AREA,NUMBER_OF_PUBLICATIONS,LATEST_YEAR_OF_PUBLICATION) values('"
						+ author.getName() + "','" + author.getUniversity() + "','" + author.getAreaOfResearch() + "','"
						+ author.getPublicationsCount() + "','" + author.getLatestYearOfPublication() + "');";

				if (statement == null) {
					statement = con.prepareStatement(sqlText);
				}
				// adding sql statements to batch
				statement.addBatch(sqlText);

			}

		} catch (Exception e) {
			logger.error(e);

		}
	}

	/**
	 * Search author by name.
	 *
	 * @param author
	 *            the author
	 * @return the result set
	 */
	// function to search author by name
	public ResultSet searchAuthorByName(String author) {
		PropertyConfigurator.configure(commons.Constants.LOG4J_PROPERTIES);
		// establishing sql connection to database
		PreparedStatement preparedStatement = null;
		con = getConnection();
		ResultSet rs = null;
		if (con != null) {

			try {
				// generating sql statement
				String sqlText;
				if (author != null && !author.isEmpty() && author.split("[ ,!?.:;%@#$^&*]+").length > 1 && author.contains("[ ,!?.:;%@#$^&*]+")) {
					String[] authorName = author.split("[ ,!?.:;%@#$^&*]+");
					sqlText = "SELECT AUTHOR_NAME FROM DBLP_SEARCH.ARTICLE WHERE  ";
					for (int i = 0; i < authorName.length; i++) {
						if (i == authorName.length - 1) {
							sqlText += "AUTHOR_NAME LIKE ? ";
						} else {
							sqlText += "AUTHOR_NAME LIKE ? OR ";
						}
					}
					sqlText += " GROUP BY AUTHOR_NAME";
					preparedStatement = con.prepareStatement(sqlText);
					for (int i = 0; i < authorName.length; i++) {
						preparedStatement.setString(i + 1, "%" + authorName[i] + "%");
					}

				} else {
					sqlText = "SELECT AUTHOR_NAME FROM DBLP_SEARCH.ARTICLE WHERE AUTHOR_NAME LIKE ?";
					preparedStatement = con.prepareStatement(sqlText);
					preparedStatement.setString(1, "%" + author.trim() + "%");
				}

				rs = preparedStatement.executeQuery();
			} catch (SQLException e) {
				logger.error(e);
			}
		} else {
			logger.error("Connection object returned null at line 492 DBUtils.java");
		}
		return rs;
	}

	/**
	 * Search journal by title.
	 *
	 * @param journal : journal Name
	 *            
	 * @return the result set
	 */
	public ResultSet searchJournalByTitle(String journal) {
		PropertyConfigurator.configure(commons.Constants.LOG4J_PROPERTIES);
		// establishing sql connection to database
		ArrayList<Article> articles = new ArrayList<Article>();
		con = getConnection();
		PreparedStatement preparedStatement = null;
		ResultSet rs = null;
		if (con != null) {

			try {

				// generating sql statement
				String sqlText;
				String regex = "[ ,!?.:;%@#$^&*]+";
				if (journal != null && (journal.split(regex).length > 0)) {
					String[] words = journal.split(regex);

					sqlText = "SELECT TEMP_1.AUTHOR_NAME, TEMP_1.JOURNAL,TEMP_1.TITLE, TEMP_1.YEAR,"
							+ "CASE TEMP_2.AUTHOR_NAME WHEN isnull(TEMP_2.author_name) THEN 'Y' ELSE 'N' END"
							+ " AS COMMITTEE_MEMBER FROM " + " (SELECT AUTHOR_NAME,JOURNAL,TITLE,ARTICLE_YEAR AS YEAR "
							+ "FROM DBLP_SEARCH.ARTICLE B " + " WHERE ";

					for (int i = 0; i < words.length; i++) {
						if (i == words.length - 1) {
							sqlText += " B.TITLE LIKE ?";
						} else {
							sqlText += " B.TITLE LIKE ? OR";
						}
					}
					sqlText += " and author_name <> ' ')TEMP_1 ";
					sqlText += " LEFT OUTER JOIN (SELECT AUTHOR_NAME, max(cast(COMMITTEE_YEAR as unsigned)) as COMMITTEE_YEAR "
							+ "FROM COMMITTEE WHERE PROGRAM_CHAIR_FLAG='T' OR "
							+ "GENERAL_CHAIR_FLAG='T' OR  CONFERENCE_CHAIR_FLAG='T' OR EXTERNAL_REVIEW_FLAG='T' group by trim(AUTHOR_NAME))TEMP_2 "
							+ "ON TEMP_1.AUTHOR_NAME = TEMP_2.AUTHOR_NAME group by temp_1.author_name,temp_1.title,temp_1.journal,temp_1.year,COMMITTEE_MEMBER";

					preparedStatement = con.prepareStatement(sqlText);
					for (int i = 0; i < words.length; i++) {
						preparedStatement.setString(i + 1, "%" + words[i] + "%");
					}

				}

				rs = preparedStatement.executeQuery();

			} catch (SQLException e) {
				logger.error(e);
			}
		} else {
			logger.error("Connection object returned null at line 492 DBUtils.java");
		}
		return rs;
	}

	/**
	 * Search conference by title.
	 *
	 * @param conf
	 *            the conf
	 * @return the result set
	 */
	public ResultSet searchConferenceByTitle(String conf) {
		PropertyConfigurator.configure(commons.Constants.LOG4J_PROPERTIES);

		con = getConnection();
		PreparedStatement preparedStatement = null;
		ResultSet rs = null;
		if (con != null) {

			try {

				// generating sql statement
				int count = 0;
				String sqlText;
				String regex = "[ ,!?.:;%@#$^&*]+";
				if (conf != null && (conf.split(regex).length > 0)) {
					String[] words = conf.split(regex);
					sqlText = "SELECT TEMP_1.AUTHOR_NAME, temp_1.TITLE,TEMP_1.YEAR,COMMITTEE_YEAR, "
							+ "CASE TEMP_2.AUTHOR_NAME WHEN isnull(TEMP_1.AUTHOR_NAME) THEN 'Y' ELSE 'N' END as CHAIR_FLAG FROM ";
					sqlText += " (SELECT B.AUTHOR_NAME,B.TITLE,B.INP_YEAR AS YEAR"
							+ " FROM  DBLP_SEARCH.INPROCEEDINGS B " + " WHERE ";
					for (String word : words) {
						sqlText += " B.TITLE LIKE ? OR ";
					}

					
					sqlText += " B.ID = ?";
					sqlText += " UNION " + " SELECT A.AUTHOR_NAME,A.TITLE,A.ARTICLE_YEAR AS YEAR "
							+ " FROM DBLP_SEARCH.ARTICLE A" + " WHERE A.ID LIKE ? OR A.TITLE LIKE ?";
					sqlText += " and author_name <> ' ')TEMP_1";

					sqlText += " LEFT OUTER JOIN (SELECT AUTHOR_NAME, max(cast(COMMITTEE_YEAR as unsigned)) as COMMITTEE_YEAR " // NOSONAR
							+ "FROM COMMITTEE WHERE PROGRAM_CHAIR_FLAG='T' OR "
							+ "GENERAL_CHAIR_FLAG='T' OR  CONFERENCE_CHAIR_FLAG='T' OR EXTERNAL_REVIEW_FLAG='T' group by trim(AUTHOR_NAME))TEMP_2 "
							+ "ON TEMP_1.AUTHOR_NAME = TEMP_2.AUTHOR_NAME "
							+ "group by TEMP_1.author_name, temp_1.title, temp_1.year, COMMITTEE_YEAR";

					preparedStatement = con.prepareStatement(sqlText);
					for (int i = 0; i < words.length; i++) {
						count++;
						preparedStatement.setString(i + 1, "%" + words[i] + "%");
					}

					preparedStatement.setString(words.length + 1, "%" + conf + "%");
					preparedStatement.setString(words.length + 2, "%" + conf + "%");
					preparedStatement.setString(words.length + 3, "%" + conf + "%");
					System.out.println(sqlText);
					System.out.println(count);

				}

				rs = preparedStatement.executeQuery();

			} catch (SQLException e) {
				logger.error(e);
			}
		} else {
			logger.error("Connection object returned null at line 492 DBUtils.java");
		}

		return rs;
	}

	/**
	 * Search domain by keyword.
	 *
	 * @param keyword
	 *            the keyword
	 * @return the result set
	 */
	public ResultSet searchDomainByKeyword(String keyword) {
		PropertyConfigurator.configure(commons.Constants.LOG4J_PROPERTIES);

		con = getConnection();
		PreparedStatement preparedStatement = null;
		ResultSet rs = null;
		if (con != null) {

			try {

				String sqlText;
				String regex = "[ ,!?.:;%@#$^&*]+";
				if (keyword != null && (keyword.split(regex).length > 1)) {
					String[] words = keyword.split(regex);
					sqlText = "SELECT B.AUTHOR_NAME,B.JOURNAL,B.TITLE,B.ARTICLE_YEAR AS YEAR "
							+ "FROM DBLP_SEARCH.ARTICLE B " + "WHERE ";

					for (int i = 0; i < words.length; i++) {
						if (i == words.length - 1) {
							sqlText += " B.TITLE LIKE ?";
						} else {
							sqlText += " B.TITLE LIKE ? OR";
						}
					}
					preparedStatement = con.prepareStatement(sqlText);
					for (int i = 0; i < words.length; i++) {
						preparedStatement.setString(i + 1, "%" + words[i] + "%");
					}
				} else {
					sqlText = "SELECT B.AUTHOR_NAME,B.JOURNAL,B.TITLE,B.ARTICLE_YEAR AS YEAR "
							+ "FROM  DBLP_SEARCH.ARTICLE B " + "WHERE B.TITLE LIKE ? ";

					preparedStatement = con.prepareStatement(sqlText);
					preparedStatement.setString(1, "%" + keyword + "%");
				}

				rs = preparedStatement.executeQuery();

			} catch (SQLException e) {
				logger.error(e);
			}
		} else {
			logger.error("Connection object returned null at line 492 DBUtils.java");
		}
		return rs;
	}

	/**
	 * Result setto table model.
	 *
	 * @param rs
	 *            the rs
	 * @return the table model
	 */
	public static TableModel resultSettoTableModel(ResultSet rs) {
		try {

			ResultSetMetaData metaData = rs.getMetaData();
			int numberOfColumns = metaData.getColumnCount();
			Vector<String> columnNames = new Vector<String>();

			// Get the column names
			for (int column = 0; column < numberOfColumns; column++) {
				columnNames.addElement(metaData.getColumnLabel(column + 1));
			}

			// Get all rows.
			Vector<Vector<Object>> rows = new Vector<Vector<Object>>();

			while (rs.next()) {
				Vector<Object> newRow = new Vector<Object>();

				for (int i = 1; i <= numberOfColumns; i++) {
					newRow.addElement(rs.getObject(i));
				}

				rows.addElement(newRow);
			}

			return new DefaultTableModel(rows, columnNames) {
				@Override
				public boolean isCellEditable(int row, int column) {
					return false;
				}
			};
		} catch (Exception e) {
			e.printStackTrace();

			return null;
		}
	}

	/**
	 * Result setto table model authors.
	 *
	 * @param rs
	 *            the rs
	 * @param fname
	 *            the fname
	 * @return the table model
	 */
	public static TableModel resultSettoTableModelAuthors(ResultSet rs, String fname) {
		try {
			HashSet<String> allNames = new HashSet<String>();
			if (fname.contains(",")) {
				String[] names = fname.split(",");
				for (String nam : names) {
					allNames.add(nam);
				}
			}
			authorPageNames = new HashMap<String, Integer>();
			ResultSetMetaData metaData = rs.getMetaData();
			int numberOfColumns = metaData.getColumnCount();
			Vector<String> columnNames = new Vector<String>();

			// Get the column names
			for (int column = 0; column < numberOfColumns; column++) {
				columnNames.addElement(metaData.getColumnLabel(column + 1));
			}
			columnNames.addElement("Number of Articles");

			// Get all rows.
			Vector<Vector<Object>> rows = new Vector<Vector<Object>>();

			while (rs.next()) {
				Vector<Object> newRow = new Vector<Object>();

				for (int i = 1; i <= numberOfColumns; i++) {
					String element = (String) rs.getObject(i);
					String[] auths = element.split(",");

					for (String author : auths) {

						if (author.toLowerCase().contains(fname.toLowerCase()) && !fname.isEmpty()
								&& !author.trim().isEmpty()) {
							if (!authorPageNames.containsKey(author.trim())) {

								newRow.addElement(author);
								authorPageNames.put(author.trim(), 1);
							} else {
								int count = authorPageNames.get(author.trim());
								count++;
								if(count >= maxCountAuthors){
									maxCountAuthors=count;
								}
								authorPageNames.put(author.trim(), count);
							}
						} else {
							boolean check = false;
							for (String s : allNames) {
								if (author.contains(s)) {
									check = true;
									break;
								}
							}
							if (check) {
								if (!authorPageNames.containsKey(author.trim())) {

									newRow.addElement(author);
									authorPageNames.put(author.trim(), 1);
								} else {
									int count = authorPageNames.get(author.trim());
									count++;
									if(count >= maxCountAuthors){
										maxCountAuthors=count;
									}
									authorPageNames.put(author.trim(), count);
								}
							}
						}

					}
				}
			}
			Set<Map.Entry<String, Integer>> entrySet = authorPageNames.entrySet();
			for (Entry entry : entrySet) {
				Vector<Object> newRow = new Vector<Object>();
				newRow.addElement(entry.getKey());
				newRow.addElement(entry.getValue());
				rows.addElement(newRow);
			}

			return new DefaultTableModel(rows, columnNames) {
				@Override
				public boolean isCellEditable(int row, int column) {
					return false;
				}
			};
		} catch (Exception e) {
			e.printStackTrace();

			return null;
		}
	}

	/**
	 * Result setto table model journal.
	 *
	 * @param rs The result Set
	 *            
	 * @return the table model
	 */
	public static TableModel resultSettoTableModelJournal(ResultSet rs) {
		try {
			authorNames = new ArrayList<String>();
			authorConfNames = new HashMap<String,Integer>();
			ResultSetMetaData metaData = rs.getMetaData();
			int numberOfColumns = metaData.getColumnCount();
			Vector<String> columnNames = new Vector<String>();

			// Get the column names
			for (int column = 0; column < numberOfColumns; column++) {
				columnNames.addElement(metaData.getColumnLabel(column + 1));
			}

			// Get all rows.
			Vector<Vector<Object>> rows = new Vector<Vector<Object>>();

			while (rs.next()) {

				String authors = (String) rs.getObject(1);
				String[] authlist = authors.split(",");

				for (String author : authlist) {
					if(!author.trim().isEmpty()){
						Vector<Object> newRow = new Vector<Object>();
						for (int i = 1; i <= numberOfColumns; i++) {

							if (i == 1)// author names comma separated column
							{
								if(authorConfNames.containsKey(author.trim())){
									Integer count = authorConfNames.get(author);
									count++;
									if(count >= maxCountConf){
										maxCountConf=count;
									}
									authorConfNames.put(author,count);
								}else {
									authorConfNames.put(author, 1);
								}
								// String element = (String) rs.getObject(i);
								newRow.addElement(author);
								// authorNames.add(element);
							} else {
								newRow.addElement(rs.getObject(i));
							}

						}
						rows.addElement(newRow);
					}
				}

			}

			return new DefaultTableModel(rows, columnNames) {
				@Override
				public boolean isCellEditable(int row, int column) {
					return false;
				}
			};
		} catch (Exception e) {
			e.printStackTrace();

			return null;
		}
	}

	/**
	 * Search author by details.
	 *
	 * @param author
	 *            the author
	 * @return the result set
	 */
	public ResultSet searchAuthorByDetails(String author) {
		PropertyConfigurator.configure(commons.Constants.LOG4J_PROPERTIES);
		// establishing sql connection to database
		PreparedStatement preparedStatement = null;
		con = getConnection();
		ResultSet rs = null;
		if (con != null) {

			try {
				// generating sql statement
				String sqlText;
				if (author != null && !author.isEmpty()) {

					sqlText = "SELECT NUMBER_OF_PUBLICATIONS ,AREA,UNIVERSITY,LATEST_YEAR_OF_PUBLICATION "
							+ "FROM DBLP_SEARCH.AUTHORS WHERE AUTHOR_NAME LIKE ? ";
					preparedStatement = con.prepareStatement(sqlText);
					preparedStatement.setString(1, "%" + author + "%");

				} else {
					sqlText = "SELECT NUMBER_OF_PUBLICATIONS,AREA,UNIVERSITY,LATEST_YEAR_OF_PUBLICATION "
							+ "FROM DBLP_SEARCH.AUTHORS WHERE AUTHOR_NAME LIKE ? limit 500 ";
					preparedStatement = con.prepareStatement(sqlText);
					preparedStatement.setString(1, "%" + author.trim() + "%");
				}

				rs = preparedStatement.executeQuery();
			} catch (SQLException e) {
				logger.error(e);
			}
		} else {
			logger.error("Connection object returned null at line 492 DBUtils.java");
		}
		return rs;
	}

	/**
	 * Search author by details in article.
	 *
	 * @param author
	 *            the author
	 * @return the result set
	 */
	public ResultSet searchAuthorByDetailsInArticle(String author) {

		PropertyConfigurator.configure(commons.Constants.LOG4J_PROPERTIES);
		// establishing sql connection to database
		PreparedStatement preparedStatement = null;
		con = getConnection();
		ResultSet rs = null;
		if (con != null) {

			try {
				// generating sql statement
				String sqlText;
				if (author != null && !author.isEmpty()) {

					sqlText = "SELECT AUTHOR_NAME as Author,TITLE,JOURNAL FROM DBLP_SEARCH.ARTICLE WHERE AUTHOR_NAME LIKE ? limit 200";
					preparedStatement = con.prepareStatement(sqlText);
					preparedStatement.setString(1, "%" + author + "%");

				} else {
					sqlText = "SELECT AUTHOR_NAME as Author,NUMBER_OF_PUBLICATIONS,AREA,UNIVERSITY,LATEST_YEAR_OF_PUBLICATION FROM DBLP_SEARCH.AUTHORS WHERE AUTHOR_NAME LIKE ? limit 500 ";
					preparedStatement = con.prepareStatement(sqlText);
					preparedStatement.setString(1, "%" + author.trim() + "%");
				}

				rs = preparedStatement.executeQuery();
			} catch (SQLException e) {
				logger.error(e);
			}
		} else {
			logger.error("Connection object returned null at line 492 DBUtils.java");
		}
		return rs;
	}

	/**
	 * Result setto table model author details.
	 *
	 * @param rs
	 *            the rs
	 * @return the table model
	 */
	public static TableModel resultSettoTableModelAuthorDetails(ResultSet rs) {
		try {
			ResultSetMetaData metaData = rs.getMetaData();
			int numberOfColumns = metaData.getColumnCount();
			Vector<String> columnNames = new Vector<String>();

			// Get the column names
			for (int column = 0; column < numberOfColumns; column++) {
				columnNames.addElement(metaData.getColumnLabel(column + 1));
			}

			// Get all rows.
			Vector<Vector<Object>> rows = new Vector<Vector<Object>>();

			while (rs.next()) {
				Vector<Object> newRow = new Vector<Object>();

				for (int i = 1; i <= numberOfColumns; i++) {
					newRow.addElement(rs.getObject(i));
				}

				rows.addElement(newRow);
			}

			return new DefaultTableModel(rows, columnNames) {
				@Override
				public boolean isCellEditable(int row, int column) {
					return false;
				}
			};
		} catch (Exception e) {
			e.printStackTrace();

			return null;
		}

	}

	/**
	 * Search similiar author by domain.
	 *
	 * @param journal
	 *            the journal
	 * @param title
	 *            the title
	 * @param authorName
	 *            the author name
	 * @return the result set
	 */

	public ResultSet searchSimiliarAuthorByDomain(String authorName) {

		PropertyConfigurator.configure(commons.Constants.LOG4J_PROPERTIES);
		// establishing sql connection to database
		PreparedStatement preparedStatement = null;
		con = getConnection();
		ResultSet rs = null;
		if (con != null) {

			try {
				// generating sql statement
				String sqlText;

				if (authorName != null && !authorName.isEmpty()) {

					sqlText = "SELECT AUTHOR_NAME FROM DBLP_SEARCH.ARTICLE WHERE "
							+ " JOURNAL IN (SELECT JOURNAL FROM dblp_search.article where AUTHOR_NAME LIKE ? group by journal) ";
					sqlText += "UNION ALL select AUTHOR_NAME FROM AUTHORS WHERE AREA IN "
							+ "(SELECT AREA FROM AUTHORS WHERE AUTHOR_NAME LIKE ?) GROUP BY AUTHOR_NAME LIMIT 1000";
					preparedStatement = con.prepareStatement(sqlText);
					preparedStatement.setString(1, "%" + authorName.trim() + "%");
					preparedStatement.setString(2, "%" + authorName.trim() + "%");
				}

				rs = preparedStatement.executeQuery();
			} catch (SQLException | NullPointerException e) {
				logger.error(e);
			}
		} else {
			logger.error("Connection object returned null at line 492 DBUtils.java");
		}
		return rs;
	}

	/**
	 * Sets the warning msg.
	 *
	 * @param text
	 *            the new warning msg
	 */
	public void setWarningMsg(String text) {
		Toolkit.getDefaultToolkit().beep();
		JOptionPane optionPane = new JOptionPane(text, JOptionPane.WARNING_MESSAGE);
		JDialog dialog = optionPane.createDialog("Warning!");
		dialog.setAlwaysOnTop(true);
		dialog.setVisible(true);
	}

	/**
	 * Sets the help msg.
	 *
	 * @param text
	 *            the new help msg
	 */
	public void setHelpMsg(String text) {
		Toolkit.getDefaultToolkit().beep();
		JOptionPane optionPane = new JOptionPane(text, JOptionPane.INFORMATION_MESSAGE);
		JDialog dialog = optionPane.createDialog("Information");
		dialog.setAlwaysOnTop(true);
		dialog.setVisible(true);
	}

	public static TableModel resultSettoTableModelSimilarAuthorDetails(ResultSet rs) {
		try {
			HashSet<String> similarAuthors = new HashSet<String>();
			ResultSetMetaData metaData = rs.getMetaData();
			int numberOfColumns = metaData.getColumnCount();
			Vector<String> columnNames = new Vector<String>();

			// Get the column names
			for (int column = 0; column < numberOfColumns; column++) {
				columnNames.addElement(metaData.getColumnLabel(column + 1));
			}

			// Get all rows.
			Vector<Vector<Object>> rows = new Vector<Vector<Object>>();

			while (rs.next()) {
				String authors = (String) rs.getObject(1);
				String[] authorList = authors.split(",");
				for (String author : authorList) {
					if (author != null && !author.trim().isEmpty()) {
						if (!similarAuthors.contains(author)) {
							Vector<Object> newRow = new Vector<Object>();

							for (int i = 1; i <= numberOfColumns; i++) {
								newRow.addElement(author);
							}

							rows.addElement(newRow);
						}
					}
				}
			}

			return new DefaultTableModel(rows, columnNames) {
				@Override
				public boolean isCellEditable(int row, int column) {
					return false;
				}
			};
		} catch (Exception e) {
			e.printStackTrace();

			return null;
		}

	}

}
