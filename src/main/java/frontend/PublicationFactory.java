/*
 * 
 */
package frontend;

import org.xml.sax.Attributes;

// TODO: Auto-generated Javadoc
/**
 * A factory for creating Publication objects. Implements Factory Design Pattern
 */
public class PublicationFactory {

	/**
	 * Gets the publication.
	 *
	 * @param publication the publication
	 * @param attributes the attributes
	 * @param publications the publications
	 * @return the publication
	 */
	public Publication getPublication(String publication,Attributes attributes,Publication publications){
		if(publication == null){
			return null;
		}
		else{
			switch(publication.trim().toLowerCase()){
			case commons.Constants.ARTICLE:
				return new Article(attributes.getValue("key"));
			case commons.Constants.INPROCEEDINGS:
				return new InProceedings(attributes.getValue("key"));
			case commons.Constants.WWW:
				return new Www(attributes.getValue("key"));
			default:
				return publications;
			}
		}
	}
}
