/*
 * 
 */
package frontend;

// TODO: Auto-generated Javadoc
/**
 * The Class Author.
 */
public class Author {
	
	/** The name. */
	private String name;
	
	/** The university. */
	private String university;
	
	/** The area of research. */
	private String areaOfResearch;
	
	/** The publications count. */
	private int publicationsCount;
	
	/** The latest year of publication. */
	private int latestYearOfPublication;
	
	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * Sets the name.
	 *
	 * @param name the new name
	 */
	public void setName(String name) {
		this.name = name;
	}
	
	/**
	 * Gets the university.
	 *
	 * @return the university
	 */
	public String getUniversity() {
		return university;
	}
	
	/**
	 * Sets the university.
	 *
	 * @param university the new university
	 */
	public void setUniversity(String university) {
		this.university = university;
	}
	
	/**
	 * Gets the area of research.
	 *
	 * @return the area of research
	 */
	public String getAreaOfResearch() {
		return areaOfResearch;
	}
	
	/**
	 * Sets the area of research.
	 *
	 * @param areaOfResearch the new area of research
	 */
	public void setAreaOfResearch(String areaOfResearch) {
		this.areaOfResearch = areaOfResearch;
	}
	
	/**
	 * Gets the publications count.
	 *
	 * @return the publications count
	 */
	public int getPublicationsCount() {
		return publicationsCount;
	}
	
	/**
	 * Sets the publications count.
	 *
	 * @param publicationsCount the new publications count
	 */
	public void setPublicationsCount(int publicationsCount) {
		this.publicationsCount = publicationsCount;
	}
	
	/**
	 * Gets the latest year of publication.
	 *
	 * @return the latest year of publication
	 */
	public int getLatestYearOfPublication() {
		return latestYearOfPublication;
	}
	
	/**
	 * Sets the latest year of publication.
	 *
	 * @param latestYearOfPublication the new latest year of publication
	 */
	public void setLatestYearOfPublication(int latestYearOfPublication) {
		this.latestYearOfPublication = latestYearOfPublication;
	}

}
