/*
 * 
 */
package frontend;

// TODO: Auto-generated Javadoc
/**
 * The Interface Publication.
 */
public interface Publication {
	
	/**
	 * Process.
	 *
	 * @param qname the qname
	 * @param value the value
	 */
	public void process(String qname,String value);
}
