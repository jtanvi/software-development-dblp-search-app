/*
 * 
 */
package frontend;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;

import org.apache.log4j.Logger;
import org.jsoup.Jsoup;

// TODO: Auto-generated Javadoc
/**
 * The Class CommitteeParser.
 */
public class CommitteeParser {

	/** The global count to store number of lines processed. */
	private static int globalCount = 0;
	
	/**
	 * function to set globalCount
	 * 
	 */
	public static void setGlobalCount(int x){
		globalCount = x;
	}

	/** The logger variable to store log4j object. */
	static final Logger logger = Logger.getLogger(CommitteeParser.class);

	/**
	 * Gets the general program chair flag.
	 *
	 * @param record
	 *            the record
	 * @return the general program chair flag
	 */
	// function to check if an author is general Chair for a committee
	public static String getGeneralProgramChairFlag(String record) {
		if ("G".equalsIgnoreCase(record)) {
			return "T";
		}
		return "F";
	}

	/**
	 * Gets the program chair flag.
	 *
	 * @param record
	 *            the record
	 * @return the program chair flag
	 */
	// function to check if an author is program Chair for a committee
	public static String getProgramChairFlag(String record) {
		if ("P".equalsIgnoreCase(record)) {
			return "T";
		}
		return "F";
	}

	/**
	 * Gets the external review flag.
	 *
	 * @param record
	 *            the record
	 * @return the external review flag
	 */
	// function to check if an author is program Chair for a committee
	public static String getExternalReviewFlag(String record) {
		if ("E".equalsIgnoreCase(record)) {
			return "T";
		}
		return "F";
	}

	/**
	 * Gets the conf chair flag.
	 *
	 * @param record
	 *            the record
	 * @return the conf chair flag
	 */
	// function to check if an author is program Chair for a committee
	public static String getConfChairFlag(String record) {
		if ("C".equalsIgnoreCase(record)) {
			return "T";
		}
		return "F";
	}

	/**
	 * Process and loads file to database.
	 *
	 * @param buffer
	 *            the bufferReader object
	 * @param fileName
	 *            the file to be parsed
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	// function to read a file and load contents to database
	public static void processAndLoad(BufferedReader buffer, String fileName) throws IOException {
		globalCount++;
		if (globalCount >= 1000) {
			DBUtils.getInstance().executeBatch();
			globalCount = 0;
		}
		String currentLine;
		String key = fileName;

		// reading individual lines from file
		while ((currentLine = buffer.readLine()) != null) {

			
				// using Jsoup to handle characters from other languages
				currentLine = Jsoup.parse(currentLine).text();
		

			globalCount++;
			// if the line contains only blanks , then ignore the current line
			if ("".equalsIgnoreCase(currentLine.replaceAll(" ", ""))) {
				continue;
			}
			// processing committee chair information from file
			if (currentLine.contains(":")) {
				String[] record = currentLine.split(":");
				String generalFlag = getGeneralProgramChairFlag(record[0]);
				String programChairFlag = getProgramChairFlag(record[0]);
				String conferenceChairFlag = getConfChairFlag(record[0]);
				String externalReviewFlag = getExternalReviewFlag(record[0]);
				Boolean otherFlag = !("T".equals(generalFlag) || "T".equals(programChairFlag)
						|| "T".equals(conferenceChairFlag) || "T".equals(externalReviewFlag));
				String authorName = record.length > 1 ? record[1] : "";
				DBUtils.getInstance().addCommitteeToDB(key, authorName, generalFlag, programChairFlag,
						conferenceChairFlag, externalReviewFlag, otherFlag.toString().substring(0, 1).toUpperCase());
			} else {
				// processing committee members from file
				DBUtils.getInstance().addCommitteeToDB(key, currentLine, "F", "F", "F", "F", "F");
			}
		}

	}

	/**
	 * Processes each file and sends the file to processAndLoad function
	 * 
	 * @param folder
	 *            the folder
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public static void parseAndLoad(final File folder) throws IOException {

		// if only file is provided as input, send it directly to processAndLoad
		if (!folder.isDirectory()) {
			BufferedReader buffer;
			try {
				buffer = new BufferedReader(new InputStreamReader(new FileInputStream(folder), "UTF-8")); // NOSONAR
				processAndLoad(buffer, folder.getName());
			} catch (FileNotFoundException e) {
				logger.error(e);
			}

			return;
		}
		// if directory then parse each file
		for (final File fileEntry : folder.listFiles()) {
			if (fileEntry.isDirectory()) {
				parseAndLoad(fileEntry);
			} else {
				BufferedReader buffer = new BufferedReader(new FileReader(fileEntry)); // NOSONAR
				processAndLoad(buffer, fileEntry.getName());
				buffer.close();
			}
		}
		runUpdate();
	}
	
	public static void runUpdate(){
		DBUtils.getInstance().updateYearInCommittee();
	}

	
	/* public static void main(String[] args) throws IOException { //NOSONAR
	  
	  final File folder = new File("committees"); //NOSONAR
	  //PropertyConfigurator.configure(commons.Constants.LOG4J_PROPERTIES);
	   parseAndLoad(folder); //NOSONAR
	  DBUtils.getInstance().executeBatch(); //NOSONAR
	  logger.info("Committee data parsed successfully"); //NOSONAR
	  
	  } //NOSONAR
	  
	  
	 */

}
