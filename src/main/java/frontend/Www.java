/*
 * 
 */
package frontend;

/***
 * 
 * @author amogh
 * @purpose : This class is used to represent an www record from dblp.xml
 * @module : parsing-engine :backend
 *
 */

//import statements
import java.util.ArrayList;
import java.util.List;

// TODO: Auto-generated Javadoc
/**
 * The Class Www.
 */
public class Www implements Publication {

	/** The www key. */
	// attributes of www record
	String wwwKey;
	
	/** The year. */
	int year;
	
	/** The title. */
	String title;
	
	/** The editor. */
	ArrayList<String> editor;
	
	/** The author. */
	ArrayList<String> author;

	// default constructor

	/**
	 * Instantiates a new www.
	 */
	public Www() {
		wwwKey = "";
		year = 0;
		title = "";
		editor = new ArrayList<>();
		author = new ArrayList<>();
	}

	/**
	 * Instantiates a new www.
	 *
	 * @param key the key
	 */
	public Www(String key) {
		this.setKey(key);
		year = 0;
		title = "";
		editor = new ArrayList<>();
		author = new ArrayList<>();
	}

	// defining getter and setter methods

	/**
	 * Sets the key.
	 *
	 * @param wwwKey the new key
	 */
	// function to set wwwKey attribute
	public void setKey(String wwwKey) {
		this.wwwKey = wwwKey;
	}

	/**
	 * Gets the key.
	 *
	 * @return the key
	 */
	// function to get wwwKey for an article object
	public String getKey() {
		return this.wwwKey;
	}

	/**
	 * Gets the editors.
	 *
	 * @return the editors
	 */
	// function to get editors
	public List<String> getEditors() {
		return this.editor;
	}

	/**
	 * Sets the editor.
	 *
	 * @param name the new editor
	 */
	// function to set editors
	public void setEditor(String name) {
		editor.add(name);
	}

	/**
	 * Gets the authors.
	 *
	 * @return the authors
	 */
	// function to get editors
	public List<String> getAuthors() {
		return this.author;
	}

	/**
	 * Sets the author.
	 *
	 * @param name the new author
	 */
	// function to set editors
	public void setAuthor(String name) {
		author.add(name);
	}

	/**
	 * Gets the year.
	 *
	 * @return the year
	 */
	// function to get year
	public int getYear() {
		return this.year;
	}

	/**
	 * Sets the year.
	 *
	 * @param year the new year
	 */
	// function to set year
	public void setYear(int year) {
		this.year = year;
	}

	/**
	 * Gets the title.
	 *
	 * @return the title
	 */
	// function to get title
	public String getTitle() {
		return this.title;
	}

	/**
	 * Sets the title.
	 *
	 * @param title the new title
	 */
	// function to set title
	public void setTitle(String title) {
		this.title = title;
	}

	/* (non-Javadoc)
	 * @see frontend.Publication#process(java.lang.String, java.lang.String)
	 */
	public void process(String qName, String value) { //NOSONAR

		// If the closing tag is www , we load the
		// records to the database
		if (commons.Constants.WWW.equalsIgnoreCase(qName.trim())) {
			loadToDB();
		}
		// if the closing tag is author , we add the author to
		// the author attribute of incollection object
		if (commons.Constants.EDITOR.equalsIgnoreCase(qName.trim())) {
			this.setEditor(value);
		}
		// if the closing Tag is title, we set the title of
		// incollection object with the value in valueText
		if (commons.Constants.TITLE.equalsIgnoreCase(qName.trim())) {
			this.setTitle(value);
		}

		// if the closing tag is year, then the year attribute
		// of incollection object is set
		if (commons.Constants.YEAR.equalsIgnoreCase(qName.trim())) {
			this.setYear(Integer.parseInt(value.trim()));
		}

		// if the closing tag is author, then the author attribute
		// of incollection object is set
		if (commons.Constants.AUTHOR.equalsIgnoreCase(qName.trim())) {
			this.setAuthor(value);
		}

	}

	/**
	 * laods the www object to database.
	 */
	public void loadToDB() {
		DBUtils.getInstance().addWwwToDb(this);
	}
}
