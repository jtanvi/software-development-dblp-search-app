/*
 * 
 */
package user_interface;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.util.ArrayList;
import java.util.Vector;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import frontend.DBUtils;

import javax.swing.DropMode;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JScrollPane;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.TransferHandler;
import javax.swing.UIManager;

import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.awt.event.ActionEvent;

// TODO: Auto-generated Javadoc
/**
 * The Class Favorites.
 */
public class Favorites extends JFrame {

	/** The content pane. */
	private JPanel contentPane;
	
	/** The favorites table. */
	private JTable favoritesTable;
	
	/** The handler. */
	private TransferHandler handler = new DataTransferHandler();
	
	/** The favorites table model. */
	private static DefaultTableModel favoritesTableModel;
	private JButton btnExport;
	private JButton btnRemoveFromList;

	
	/**
	 * Gets the favorites table.
	 *
	 * @return the favorites table
	 */
	public JTable getFavoritesTable() {
		return favoritesTable;
	}
	

	/**
	 * Launch the application.
	 *
	 * @param args the arguments
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					UIManager.setLookAndFeel("com.jtattoo.plaf.smart.SmartLookAndFeel");
		            // start application
		     
					Favorites frame = new Favorites();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Favorites() {
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(1020, 20, 400, 607);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		
		JScrollPane scrollPane = new JScrollPane();
		
		btnRemoveFromList = new JButton("Remove From List");
		btnRemoveFromList.setEnabled(false);
		btnRemoveFromList.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnRemoveActionPerformed(favoritesTable);
				btnRemoveFromList.setEnabled(false);
			}

		});
		
		btnExport = new JButton("Export");
		btnExport.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnExportActionPerformed();	
			}
		});
		GroupLayout gl_contentPane = new GroupLayout(contentPane);
		gl_contentPane.setHorizontalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_contentPane.createSequentialGroup()
							.addGap(124)
							.addComponent(btnExport)
							.addGap(18)
							.addComponent(btnRemoveFromList))
						.addGroup(gl_contentPane.createSequentialGroup()
							.addGap(12)
							.addComponent(scrollPane, GroupLayout.PREFERRED_SIZE, 364, GroupLayout.PREFERRED_SIZE)))
					.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
		);
		gl_contentPane.setVerticalGroup(
			gl_contentPane.createParallelGroup(Alignment.TRAILING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
					.addComponent(scrollPane, GroupLayout.PREFERRED_SIZE, 518, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
						.addComponent(btnRemoveFromList)
						.addComponent(btnExport)))
		);
		
		//Vector<String> columnNames = new Vector<String>();
		//columnNames.addElement("Authors");
		//favoritesTableModel = new DefaultTableModel(null, columnNames) {
		favoritesTableModel = new DefaultTableModel(new Object[] { "Authors" }, 0) {
			@Override
			public boolean isCellEditable(int row, int column) {
				return false;
			}
		};
		favoritesTable = new JTable(favoritesTableModel);
		//favoritesTable.setDropMode(DropMode.INSERT_ROWS);
		favoritesTable.setTransferHandler(handler);
		favoritesTable.setDragEnabled(true);
		favoritesTable.setFillsViewportHeight(true);
		favoritesTable.getSelectionModel().setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
		favoritesTable.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
			@Override
			public void valueChanged(ListSelectionEvent e) {
				ListSelectionModel lsm = (ListSelectionModel) e.getSource();
				btnRemoveFromList.setEnabled(!lsm.isSelectionEmpty());
				// btnAdd.setEnabled(true);
			}
		});
		scrollPane.setViewportView(favoritesTable);
		contentPane.setLayout(gl_contentPane);
	}
	
	
	public void btnExportActionPerformed() {
		try {
			if(favoritesTable.getRowCount()>0){
				OpenFileChooser();
			}
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}
	
	
	public void btnRemoveActionPerformed(JTable table) {
		((DataTransferHandler) handler).removeSelectedRows(favoritesTable);
		btnRemoveFromList.setEnabled(false);
	}
	
	/**
     * Open file chooser.
     *
     * @return the string
     * @throws IOException Signals that an I/O exception has occurred.
     */
    public String OpenFileChooser() throws IOException
    {
    	JFileChooser fileChooser = new JFileChooser();
		fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
		int result = fileChooser.showSaveDialog(this);
		if (result == JFileChooser.APPROVE_OPTION) {
		    File selectedFile = fileChooser.getSelectedFile();
		    System.out.println("Selected file: " + selectedFile.getAbsolutePath());
		    String path=selectedFile.getAbsolutePath();
		    exportToCsv(path);
		    return selectedFile.getAbsolutePath();
		    
		    
		}
		return "Error retreiving path";
    }
    
    /**
     * Export to csv.
     *
     * @param path the path
     * @throws IOException Signals that an I/O exception has occurred.
     */
    public void exportToCsv(String path) throws IOException
    {
    	
    	ArrayList<String> authors = new ArrayList<String>();
    	
    	TableModel t = favoritesTable.getModel();
    	for(int i=0;i<t.getRowCount();i++){
    		for(int j=0;j<t.getColumnCount();j++){
    			authors.add((String)(t.getValueAt(i, j)));
    		}
    	}
    	HSSFWorkbook workbook = new HSSFWorkbook();
		HSSFSheet sheet = workbook.createSheet("AuthorName");
		
		int i=0;
		for (String name : authors){
		System.out.println("name is :");
	       System.out.println(name);
	       HSSFRow row = sheet.createRow(++i);
			HSSFCell cell = row.createCell(0);
			cell.setCellValue(name);
	       
	    }
		sheet.autoSizeColumn(1);
		
		try {
			workbook.write(new FileOutputStream( new File (path+"/authornamesexported.csv")));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		workbook.close();
    	
    }
}
