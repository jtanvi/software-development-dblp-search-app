package user_interface;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.FlowLayout;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.ListSelectionModel;
import javax.swing.RowFilter;
import javax.swing.TransferHandler;
import javax.swing.UIManager;
import javax.swing.RowFilter.Entry;
import javax.swing.JLabel;
import javax.swing.JComboBox;
import javax.swing.border.TitledBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;

import frontend.DBUtils;

import javax.swing.DefaultComboBoxModel;
import javax.swing.DropMode;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Font;
import javax.swing.JTabbedPane;
import javax.swing.JCheckBox;

// TODO: Auto-generated Javadoc
/**
 * The Class AuthorsDetailsPage.
 */
public class AuthorsDetailsPage extends JFrame {

	/** The content pane. */
	private JPanel contentPane;
	
	/** The author details table. */
	private JTable authorDetailsTable;
	
	/** The similiar authors table. */
	private JTable similiarAuthorsTable;
	
	/** The lbl similiar authors. */
	private JLabel lblSimiliarAuthors;
	
	/** The lbl new label 1. */
	private JLabel lblNewLabel_1;
	
	/** The lbl new label 2. */
	private JLabel lblNewLabel_2;
	
	/** The fav. */
	private ResultsPage page = new ResultsPage();
	private Favorites fav = page.fav; 
	
	/** The handler. */
	private TransferHandler handler = new DataTransferHandler();
	
	/** The btn add. */
	private JButton btnAdd;
	
	/** The btn favorites. */
	private JButton btnFavorites;
	
	/** Placeholder String. */
	private String name = "Author";
	
	/** The similiar author scroll pane. */
	private JScrollPane similiarAuthorScrollPane;
	private String simAuthorName="";
	private JScrollPane scrollPane;
	private JTable authorAdditionalDetailsTable;
	private JPanel activityIndicatorPanel;

	private JActivityIndicator act;

	/**
	 * Launch the application.
	 *
	 * @param args the arguments
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					 // select Look and Feel
		            UIManager.setLookAndFeel("com.jtattoo.plaf.smart.SmartLookAndFeel");
		            // start application
		           AuthorsDetailsPage frame = new AuthorsDetailsPage();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public AuthorsDetailsPage() {
		
		initComponents(name);
		
	}
	
	public JTable getSimilarAuthorsTable() {
		return this.similiarAuthorsTable;
	}
	
	//call is the number that represents from which tab this page is called 
	//1:called from author
	/**
	 * Instantiates a new authors details page.
	 *
	 * @param queryParams the query params
	 * @param call the call
	 * @throws SQLException the SQL exception
	 */
	//2.called from journal
 public AuthorsDetailsPage(String []queryParams,int call) throws SQLException {
		
	 initComponents(queryParams[0]);
	 act.startRotating();
		
		if (call==0)
		{   
			simAuthorName=queryParams[0];
			setAuthorDetailTable( queryParams[0]);
			setSimiliarAuthorDomainTable(simAuthorName);
			
			
		}
		
		
	    authorDetailsTable.addMouseListener(new MouseAdapter() {
	    	
	    	/* (non-Javadoc)
	    	 * @see java.awt.event.MouseAdapter#mouseClicked(java.awt.event.MouseEvent)
	    	 */
	    	@Override
            public void mouseClicked(MouseEvent e) {
	    		
//	    		int column1 = 1;
//	    		int column2 =2;
//        		int row = authorDetailsTable.getSelectedRow();
//        		String journal = authorDetailsTable.getModel().getValueAt(row, column2).toString();
//        		String title = authorDetailsTable.getModel().getValueAt(row, column1).toString();
	    
            }

            });	
		
		
	}
	
/**
 * Sets the author detail table.
 *
 * @param authorname the new author detail table
 * @throws SQLException the SQL exception
 */
public void setAuthorDetailTable(String authorname) throws SQLException
{
	
	ResultSet rs = DBUtils.getInstance().searchAuthorByDetailsInArticle(authorname);
	if(rs!=null){
		
		authorDetailsTable.setModel(DBUtils.resultSettoTableModelAuthorDetails(rs));
	}
  
	ResultSet rs2 = DBUtils.getInstance().searchAuthorByDetails(authorname);
	if(rs2!=null)
	{
		authorAdditionalDetailsTable.setModel(DBUtils.resultSettoTableModelAuthorDetails(rs2));
	}
	
	   	
} 


/**
 * Sets the similiar author domain table.
 *
 * @param journal the journal
 * @param title the title
 * @param authorName the author name
 */
public void setSimiliarAuthorDomainTable(String authorName)
{
	ResultSet rs = DBUtils.getInstance().searchSimiliarAuthorByDomain(authorName);
	
	if(rs!=null){
		act.stopRotating();
		similiarAuthorsTable.setModel(DBUtils.resultSettoTableModelSimilarAuthorDetails(rs));
	}
}

	

	/**
	 * Inits the components.
	 */
	private void initComponents(String authorName) {
		
		setBounds(0, 20, 1017, 607);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		String[] years = {"1","2","3","4","5"};
		List<String> list = Arrays.asList(years);
		Collections.reverse(list);
		years = (String[]) list.toArray();
	  
	  btnAdd = new JButton("Add To Favorites");
	  btnAdd.setEnabled(false);
	  btnAdd.addActionListener(new ActionListener() {
	  	public void actionPerformed(ActionEvent e) {
	  		((DataTransferHandler) handler).copyData(similiarAuthorsTable, fav.getFavoritesTable());
	  		btnAdd.setEnabled(false);
	  	}
	  });
	  btnAdd.setBounds(556, 10, 150, 40);
	  contentPane.add(btnAdd);
	  
	  btnFavorites = new JButton("Show Favorites");
	  btnFavorites.addActionListener(new ActionListener() {
	  	public void actionPerformed(ActionEvent e) {
	  		fav.setVisible(true);
	  	}
	  });
	  btnFavorites.setBounds(713, 12, 172, 37);
	  contentPane.add(btnFavorites);
	  
	  JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
	  tabbedPane.setBounds(31, 54, 980, 503);
	  contentPane.add(tabbedPane);
	  
	  JPanel similiarAuthors = new JPanel();
	  tabbedPane.addTab("Author Details", null, similiarAuthors, null);
	  similiarAuthors.setLayout(null);
	  
	  
	  JScrollPane authorDeatailScrollPane = new JScrollPane();
	  authorDeatailScrollPane.setBounds(6, 57, 956, 394);
	  similiarAuthors.add(authorDeatailScrollPane);
	  
	  authorDetailsTable = new JTable();
	  authorDetailsTable.getSelectionModel().setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
	  authorDeatailScrollPane.setViewportView(authorDetailsTable);
	  
	  
	  lblSimiliarAuthors = new JLabel("Similiar Authors:");
	  lblSimiliarAuthors.setBounds(6, 6, 172, 16);
	  similiarAuthors.add(lblSimiliarAuthors);
	  lblSimiliarAuthors.setFont(new Font("Lucida Grande", Font.PLAIN, 16));
	  
	  lblNewLabel_1 = new JLabel("(Drag row(s) or Select row(s) and click \"Add To Favorites\")");
	  lblNewLabel_1.setBounds(6, 29, 377, 16);
	  similiarAuthors.add(lblNewLabel_1);
	  
	  JPanel panel_2 = new JPanel();
	  tabbedPane.addTab("Similiar Authors", null, panel_2, null);
	  panel_2.setLayout(null);
	  
	  similiarAuthorScrollPane = new JScrollPane();
	  similiarAuthorScrollPane.setBounds(6, 34, 956, 417);
	  panel_2.add(similiarAuthorScrollPane);
	  
	  similiarAuthorsTable = new JTable();
	  similiarAuthorScrollPane.setViewportView(similiarAuthorsTable);
	  similiarAuthorsTable.getSelectionModel().setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
	  similiarAuthorsTable.setTransferHandler(handler);
	  //similiarAuthorsTable.setDropMode(DropMode.INSERT_ROWS);
	  similiarAuthorsTable.setDragEnabled(true);
	  similiarAuthorsTable.setFillsViewportHeight(true);
	  
	  lblNewLabel_2 = new JLabel("Author Details");
	  lblNewLabel_2.setBounds(6, 6, 128, 16);
	  panel_2.add(lblNewLabel_2);
	  similiarAuthorsTable.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
	  	@Override
	  	public void valueChanged(ListSelectionEvent e) {
	  		ListSelectionModel lsm = (ListSelectionModel)e.getSource();
	  		btnAdd.setEnabled(!lsm.isSelectionEmpty());
	  		//btnAdd.setEnabled(true);
	  	}
	  });
	  
	  
	 
	  similiarAuthorsTable.addMouseMotionListener(new MouseMotionListener(){

            public void mouseDragged(MouseEvent arg0) {
                fav.setVisible(true);
            }

            public void mouseMoved(MouseEvent arg0){}
      });
	  
	  JPanel panel_3 = new JPanel();
	  tabbedPane.addTab("Additional Author Information", null, panel_3, null);
	  panel_3.setLayout(null);
	  
	  scrollPane = new JScrollPane();
	  scrollPane.setBounds(6, 47, 962, 404);
	  panel_3.add(scrollPane);
	  
	  authorAdditionalDetailsTable = new JTable();
	  scrollPane.setViewportView(authorAdditionalDetailsTable);
	  
	  activityIndicatorPanel = new JPanel();
	  activityIndicatorPanel.setBounds(897, 10, 80, 40);
	  contentPane.add(activityIndicatorPanel);
	    act = new JActivityIndicator(0);
		activityIndicatorPanel.setLayout(new FlowLayout());
		act.setBounds(40, 50, 100, 80);
		activityIndicatorPanel.add(act);
		
		JLabel lblNewLabel = new JLabel(authorName);
		lblNewLabel.setFont(new Font("Lucida Grande", Font.BOLD, 17));
		lblNewLabel.setBounds(41, 15, 452, 29);
		contentPane.add(lblNewLabel);
		act.setVisible(false);
		
	}

	/**
	 * Filter publications.
	 *
	 * @param e the e
	 */
	protected void filterPublications(ActionEvent e) {
	  	
		
	}
}
