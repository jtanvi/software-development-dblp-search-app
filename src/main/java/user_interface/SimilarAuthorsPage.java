/*
 * 
 */
package user_interface;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;
import frontend.DBUtils;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.TransferHandler;

import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.awt.event.ActionEvent;
import javax.swing.UIManager;
import java.awt.Color;
import java.awt.Dimension;
// TODO: Auto-generated Javadoc

/**
 * The Class SimilarAuthorsPage.
 */
public class SimilarAuthorsPage extends JFrame {

	/** The content pane. */
	private JPanel contentPane;
	
	/** The co authors table. */
	private JTable coAuthorsTable;
	
	/** The abc. */
	private String abc="";
	
	/** The fav. */
	private Favorites fav = new Favorites();
	
	/** The handler. */
	private TransferHandler handler = new DataTransferHandler();
	
	/**
	 * Gets the abc.
	 *
	 * @return the abc
	 */
	public String getAbc() {
		return abc;
	}

	/**
	 * Sets the abc.
	 *
	 * @param abc the new abc
	 */
	public void setAbc(String abc) {
		this.abc = abc;
	}

	/**
	 * Gets the similiar authors.
	 *
	 * @return the similiar authors
	 */
	public ArrayList<String> getSimiliarAuthors() {
		return similiarAuthors;
	}

	/**
	 * Sets the similiar authors.
	 *
	 * @param similiarAuthors the new similiar authors
	 */
	public void setSimiliarAuthors(ArrayList<String> similiarAuthors) {
		this.similiarAuthors = similiarAuthors;
	}

	/** The similiar authors. */
	public static  ArrayList<String> similiarAuthors=new ArrayList<String>();
	
	/** The similiar authors table. */
	private JTable similiarAuthorsTable;
	
	/** The similiar domain authors table. */
	private JTable similiarDomainAuthorsTable;
	

	/**
	 * Launch the application.
	 *
	 * @param args the arguments
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					
					 UIManager.setLookAndFeel("com.jtattoo.plaf.smart.SmartLookAndFeel");
					SimilarAuthorsPage frame = new SimilarAuthorsPage(similiarAuthors,"");
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	/**
	 * Instantiates a new similar authors page.
	 *
	 * @param authornames the authornames
	 * @param abc the abc
	 */
	public SimilarAuthorsPage(ArrayList authornames,String abc) {
		initComponents( authornames,abc);
	}
	

	/**
	 * Create the frame.
	 *
	 * @param authornames the authornames
	 * @param abc the abc
	 */
	private void initComponents(ArrayList authornames,String abc) {
		this.setPreferredSize(new Dimension(900, 600));
		this.abc=abc;
		this.similiarAuthors=authornames;
		setBounds(100, 100, 900, 662);
		contentPane = new JPanel();
		contentPane.setBackground(Color.LIGHT_GRAY);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		
		JButton btnBack = new JButton("Back");
		btnBack.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				backActionPerformed(evt);
			}
		});
		
		JScrollPane scrollPane = new JScrollPane();
		
		JScrollPane similiarAuthorsScrollPane = new JScrollPane();
		
		JScrollPane scrollPane_1 = new JScrollPane();
		GroupLayout gl_contentPane = new GroupLayout(contentPane);
		gl_contentPane.setHorizontalGroup(
			gl_contentPane.createParallelGroup(Alignment.TRAILING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_contentPane.createSequentialGroup()
							.addContainerGap()
							.addComponent(btnBack))
						.addGroup(gl_contentPane.createSequentialGroup()
							.addGap(28)
							.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
								.addComponent(scrollPane_1, GroupLayout.PREFERRED_SIZE, 829, GroupLayout.PREFERRED_SIZE)
								.addGroup(gl_contentPane.createSequentialGroup()
									.addComponent(scrollPane, GroupLayout.PREFERRED_SIZE, 248, GroupLayout.PREFERRED_SIZE)
									.addGap(18)
									.addComponent(similiarAuthorsScrollPane, GroupLayout.PREFERRED_SIZE, 540, GroupLayout.PREFERRED_SIZE)))))
					.addContainerGap(33, Short.MAX_VALUE))
		);
		gl_contentPane.setVerticalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addContainerGap()
					.addComponent(btnBack)
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
						.addComponent(scrollPane, GroupLayout.PREFERRED_SIZE, 134, GroupLayout.PREFERRED_SIZE)
						.addComponent(similiarAuthorsScrollPane, GroupLayout.PREFERRED_SIZE, 134, GroupLayout.PREFERRED_SIZE))
					.addGap(32)
					.addComponent(scrollPane_1, GroupLayout.DEFAULT_SIZE, 370, Short.MAX_VALUE)
					.addGap(47))
		);
		
		similiarDomainAuthorsTable = new JTable();
		scrollPane_1.setViewportView(similiarDomainAuthorsTable);
		
		similiarAuthorsTable = new JTable();
		similiarAuthorsScrollPane.setViewportView(similiarAuthorsTable);
		
		coAuthorsTable = new JTable();
		Object rowData[] = new Object[4];
	    Object columnNames[] = { "Co-Authors" };
		DefaultTableModel model = new DefaultTableModel(columnNames, 0);
		
		String[] tokens = abc.split(",");
		
		
		for (int i=0;i< tokens.length;i++)
		{
			if (!(tokens[i].equalsIgnoreCase("")))
			{
			rowData[0]=tokens[i];
			model.addRow(rowData);	
			}
		}
		
		
		
//		ResultSet rs = DBUtils.getInstance().searchAuthorByDetails(abc);
//		if(rs!=null){
//			similiarAuthorsTable.setModel(DBUtils.resultSettoTableModelAuthorDetails(rs));
//		}
		
		
		
		
		coAuthorsTable.setModel(model);
		scrollPane.setViewportView(coAuthorsTable);
		contentPane.setLayout(gl_contentPane);
		
		
		coAuthorsTable.addMouseListener(new MouseAdapter() {

            @Override

            public void mouseClicked(MouseEvent e) {

            int column = 0;
            int row = coAuthorsTable.getSelectedRow();
            String value = coAuthorsTable.getModel().getValueAt(row, column).toString();
            System.out.print("On Click OF coAuthor Table:ResultsPage.java:Line 187"+value);
            setAuthorDetailTable (value.trim());
            setSimiliarAuthorDomainTable("machine", "machine","machine");
          }

            });	
		
		
		
	}
	
	/**
	 * Sets the author detail table.
	 *
	 * @param authorname the new author detail table
	 */
	public void setAuthorDetailTable(String authorname)
	{
		ResultSet rs = DBUtils.getInstance().searchAuthorByDetails(authorname);
		if(rs!=null){
			similiarAuthorsTable.setModel(DBUtils.resultSettoTableModelAuthorDetails(rs));
		}
		
	} 
	

	/**
	 * Sets the similiar author domain table.
	 *
	 * @param journal the journal
	 * @param title the title
	 * @param authorName is the name of the author 
	 */
	
	public void setSimiliarAuthorDomainTable(String journal, String title, String authorName)

	{
		//ResultSet rs = DBUtils.getInstance().searchSimiliarAuthorByDomain(journal,title, authorName);
		
		/*if(rs!=null){
			similiarDomainAuthorsTable.setModel(DBUtils.resultSettoTableModelAuthorDetails(rs));
		}*/
	}
	
	/**
	 * Back action performed.
	 *
	 * @param evt the evt
	 */
	private void backActionPerformed(java.awt.event.ActionEvent evt) {
		this.setVisible(false);
		//new ResultsPage().setVisible(true); 
	}
}
