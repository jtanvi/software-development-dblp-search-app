/*
 * 
 */
package user_interface;

import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.StringSelection;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.awt.dnd.DragSource;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Vector;

import javax.activation.ActivationDataFlavor;
import javax.activation.DataHandler;
import javax.swing.DefaultListModel;
import javax.swing.DropMode;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.TransferHandler;
//import java.swt.datatransfer.TransferHandler;
import javax.swing.table.DefaultTableModel;


// TODO: Auto-generated Javadoc
/**
 * The Class DataTransferHandler.
 */
public class DataTransferHandler extends TransferHandler{
		
	/** The local object flavor. */
	private final DataFlavor localObjectFlavor;
    
    /** The rows. */
    private int[] rows;
    
    /** The add index. */
    private int addIndex = -1; //Location where items were added
    
    /** The add count. */
    private int addCount; //Number of items added.
    
    /** The fav. */
    private JComponent fav;
		
	/**
	 * Instantiates a new data transfer handler.
	 */
	public DataTransferHandler() {
        super();
        localObjectFlavor = new ActivationDataFlavor(Object[].class, DataFlavor.javaJVMLocalObjectMimeType, "Array of items");
    }
    
    /* (non-Javadoc)
     * @see javax.swing.TransferHandler#getSourceActions(javax.swing.JComponent)
     */
    @Override 
    public int getSourceActions(JComponent c) {
    	System.out.println("error");
        return TransferHandler.COPY_OR_MOVE; 
    }
    
    /* (non-Javadoc)
     * @see javax.swing.TransferHandler#createTransferable(javax.swing.JComponent)
     */
    @Override 
    protected Transferable createTransferable(JComponent c) {
    	JTable table = (JTable) c;      
        // Get entire row data instead of just author names 
        DefaultTableModel model = (DefaultTableModel) table.getModel();
        ArrayList<Object> list = new ArrayList<>();
        rows = table.getSelectedRows();
        for (int row: rows) 
            list.add(model.getDataVector().get(row));        
        table.removeRowSelectionInterval(rows[0], rows[rows.length - 1]);
        Object[] transferedObjects = list.toArray();
        return new DataHandler(transferedObjects, localObjectFlavor.getMimeType());
        // return new StringSelection(l.getSelectedValue().toString());
    }
    
    /* (non-Javadoc)
     * @see javax.swing.TransferHandler#exportDone(javax.swing.JComponent, java.awt.datatransfer.Transferable, int)
     */
    @Override
    protected void exportDone(JComponent c, Transferable data, int action) {
    	// Uncomment below code when drag objects are to be removed from the Source Table
    	//cleanup(c, action == TransferHandler.MOVE);
    }
		
	
    /* (non-Javadoc)
     * @see javax.swing.TransferHandler#canImport(javax.swing.TransferHandler.TransferSupport)
     */
    @Override
	public boolean canImport(TransferSupport supp) {
    	JTable table = (JTable) supp.getComponent();
        boolean isDropable = supp.isDrop() && supp.isDataFlavorSupported(localObjectFlavor);
        table.setCursor(isDropable ? DragSource.DefaultMoveDrop : DragSource.DefaultMoveNoDrop);
        return isDropable;
	}
		
    /* (non-Javadoc)
     * @see javax.swing.TransferHandler#importData(javax.swing.TransferHandler.TransferSupport)
     */
    @Override
	public boolean importData(TransferSupport supp) {
    	if (!canImport(supp)) {
            return false;
        }
        TransferHandler.DropLocation tdl = supp.getDropLocation();
        if (!(tdl instanceof JTable.DropLocation)) {
            return false;
        }
        JTable.DropLocation dl = (JTable.DropLocation) tdl;
        JTable target = (JTable) supp.getComponent();
        DefaultTableModel model = (DefaultTableModel) target.getModel();
        int index = dl.getRow();
        int max = model.getRowCount();
        index = max;
        addIndex = index;
        target.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
        try {
            Object[] values = (Object[]) supp.getTransferable().getTransferData(localObjectFlavor);
            if (Objects.equals(supp, target)) {
                addCount = values.length;
            }
            for (int i = 0; i < values.length; i++) {
            	if (!existsInTable(target, values[i])) {
            		String str = values[i].toString();	//
            		String[] arry = str.split(", ");	//
            		String name = "";
            		if (arry.length > 1)
            			name = arry[0].substring(1, arry[0].length());
            		else
            			name = arry[0].substring(1);	//
            		ArrayList list = new ArrayList();	//
            		Object[] data = {};					//
            		list.add(name);						//
            		data = list.toArray();				//
	                int idx = index++;
	                //model.insertRow(idx, (Vector) values[i]);
	                model.insertRow(idx, data);			//
            	}
            }
            return true;
        } catch (UnsupportedFlavorException | IOException ex) {
            ex.printStackTrace();
        }
        return false;		
	}
		
    
    
    
    /**
     * Copy data.
     *
     * @param source the source
     * @param target the target
     */
    public void copyData(JTable source, JTable target) {
    	DefaultTableModel sourceModel = (DefaultTableModel) source.getModel();
    	DefaultTableModel targetModel = (DefaultTableModel) target.getModel();
        int sourceIndices[] = source.getSelectedRows();
        int targetIndex = targetModel.getRowCount();
        
        for (int i: sourceIndices) {
        	Object curEntry = source.getValueAt(i, 0); 
        	if (!existsInTable(target, curEntry)) {
	        	int idx = targetIndex++;
	        	targetModel.insertRow(idx, (Vector) sourceModel.getDataVector().get(i));
	        	
        	}
        }
        source.removeRowSelectionInterval(sourceIndices[0], sourceIndices[sourceIndices.length - 1]);
        //source.clearSelection();
        // uncomment when rows need to be deleted from the table
        //removeSelectedRows(source);
    }   
    
    /**
     * Removes the selected rows.
     *
     * @param table the table
     */
    public void removeSelectedRows(JTable table){
	    int numRows = table.getSelectedRows().length;
	    for(int i=0; i<numRows ; i++ ) {
	    	((DefaultTableModel) table.getModel()).removeRow(table.getSelectedRow());
	    }
	}
	 
    
    /**
     * Exists in table.
     *
     * @param table the table
     * @param entry the entry
     * @return true, if successful
     */
    public boolean existsInTable(JTable table, Object entry) {

        // Get row count
        int rowCount = table.getRowCount();
        int colCount = table.getColumnCount();
        String curEntry = entry.toString();
        //String[] cur = curEntry.split("[ ,!?.:;%@#$^&*]+");
        String[] cur = curEntry.split(",");
        curEntry = cur[0].substring(1);
        System.out.println(curEntry + "===>");

        // Check against all entries
        for (int i = 0; i < rowCount; i++) {
        	String rowEntry1 = table.getValueAt(i, 0).toString();
            String rowEntry2 = "[" + table.getValueAt(i, 0).toString() + "]";
            System.out.println(rowEntry1);
	        if (rowEntry1.equalsIgnoreCase(curEntry) || rowEntry2.equalsIgnoreCase(curEntry)) {
	            return true;
	        }
        }
        return false;
    }

    
    public Object getTransferableObject(JTable table) {
    	return createTransferable(table);
    }
    
    
    /**
     * Cleanup.
     *
     * @param c the c
     * @param remove the remove
     * Uncomment when drag objects are to be removed from the Source Table
    private void cleanup(JComponent c, boolean remove) {
        if (remove && rows != null) {
            c.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
            DefaultTableModel model = (DefaultTableModel) ((JTable) c).getModel();
            if (addCount > 0) {
                for (int i = 0; i < rows.length; i++) {
                    if (rows[i] >= addIndex) {
                    	rows[i] += addCount;
                    }
                }
            }
            for (int i = rows.length - 1; i >= 0; i--) {
                model.removeRow(rows[i]);
            }
        }
        rows  = null;
        addCount = 0;
        addIndex = -1;
    }
    
    */

    
} 

