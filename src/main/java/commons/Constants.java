
package commons;

// TODO: Auto-generated Javadoc
/**
 * The Class Constants.
 * 
 */
public class Constants { //NOSONAR

	/** The Constant AUTHOR which stores name of author tag. */
 public static final String AUTHOR = "author";
	
	/** The Constant TITLE. stores name of title tag */
	public static final String TITLE = "title";
	
	// password used for testing
	public static final String TestPassword = "@Mogh123";
	
	//username for tesing
	public static final String TestUsername = "root";
	
	
	/** The Constant SCHOOL. */
	public static final String SCHOOL = "school";
	
	/** The Constant YEAR. */
	public static final String YEAR = "year";
	
	/** The Constant MONTH. */
	public static final String MONTH = "month";
	
	/** The Constant JOURNAL. */
	public static final String JOURNAL = "journal";
	
	/** The Constant VOLUME. */
	public static final String VOLUME = "volume";
	
	/** The Constant ISBN. */
	public static final String ISBN = "ISBN";
	
	public static final String BOOK_TITLE = "booktitle";  

	
	/** The Constant INPROCEEDINGS. */
	public static final String INPROCEEDINGS = "inproceedings";
	
	/** The Constant ARTICLE. */
	public static final String ARTICLE = "article";
	
	/** The Constant INCOLLECTION. */
	public static final String INCOLLECTION = "incollection";
	
	/** The Constant WWW. */
	public static final String WWW = "www";
	

	/** The Constant EDITOR. */
	public static final String EDITOR = "editor";
	
	/** The Constant LOG4J_PROPERTIES stores the name of the log4j properties file */
	public static final String LOG4J_PROPERTIES = "log4j.properties";
	
	
	/** The Constant JDBC_DRIVER. used to store driver string */
	public static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
	
	/** The Constant AUTHORS_FILE. */
	public static final String AUTHORS_FILE ="generated-author-info.csv";
	

	

}
