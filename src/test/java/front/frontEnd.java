/*
 * 
 */
package front;

import static org.junit.Assert.*;

import org.junit.Assert;
import org.junit.Test;

import frontend.Article;
import frontend.InProceedings;
import frontend.MyParser;
import frontend.PublicationFactory;
import frontend.Www;

// TODO: Auto-generated Javadoc
/**
 * The Class frontEnd.
 */
public class frontEnd {

	/**
	 * Test my parser.
	 */
	@Test
	public void testMyParser() {
		new MyParser("dblp_test1.xml", "committee_test1/ase2001-pc.txt");
		// testing the case where file path for committee is provided instead of
		// directory path
		Assert.assertTrue(MyParser.readData());
	}

	
	/**
	 * Test 1.
	 */
	@Test
	public void test1() {
		new MyParser("dblp_test1.xml", "committee_test1");
		Assert.assertEquals(true, MyParser.readData());
	}

	/**
	 * Test 2.
	 */
	@Test
	public void test2() {
		new MyParser("dblp_test1.xml", "committee_test11");
		Assert.assertEquals(true, MyParser.readData());
	}

	/**
	 * Test 3.
	 */
	@Test
	public void test3() {
		new MyParser("dblp_test1.xml", "committee_test11");
		Assert.assertEquals(true, MyParser.readData());
	}

	/**
	 * Test 4.
	 */
	@Test
	public void test4() {
		new MyParser("abc.xml", "def");
		// wrong paths for both dblp and committee
		Assert.assertEquals(false, MyParser.readData());
	}

	/**
	 * Test 5.
	 */
	@Test
	public void test5() {
		new MyParser("dblp_test1.xml", "committee_test1/ase2001-pc.txt");
		// testing the case where file path for committee is provided instead of
		// directory path
		Assert.assertTrue(MyParser.readData());
	}

	/**
	 * Test 6.
	 */
	@Test
	public void test6() {
		new MyParser("dblp_test1.xml", "committees_test2");
		// testing the case where file path for committee is provided instead of
		// directory path
		Assert.assertTrue(MyParser.readData());
	}

	/**
	 * Test 7.
	 */
	@Test
	public void test7() {
		new MyParser("dblp_text3.xml", "committee_test1");
		// testing the case where file path for committee is provided instead of
		// directory path
		Assert.assertFalse(MyParser.readData());
	}
	
	/**
	 * Test 8.
	 */
	@Test
	public void test8() {
		Assert.assertTrue(new Www().getYear()==0);
	}
	
	/**
	 * Test 9.
	 */
	@Test
	public void test9() {
		Assert.assertTrue(new InProceedings().getYear()==0);
	}
	
	
	/**
	 * Test 10.
	 */
	@Test
	public void test10() {
		Assert.assertTrue(new PublicationFactory().getPublication("mastersthesis",null,null)==null);
	}
	
	/**
	 * Test 11.
	 */
	@Test
	public void test11() {
		Assert.assertTrue(new PublicationFactory().getPublication(null,null,null)==null);
	}
	
	/**
	 * Test 12.
	 */
	@Test(expected = NullPointerException.class)
	public void test12() {
		Assert.assertFalse(new PublicationFactory().getPublication("article",null,null)==null);
	}
	
	/**
	 * Test 13.
	 */
	@Test
	public void test13() {
		Assert.assertTrue(new Article().getYear()==0);
	}
	
	/**
	 * Test 14.
	 */
	@Test
	public void test14() {
		//Assert.assertTrue();
	}
	
	/**
	 * Test 15.
	 */
	@Test
	public void test15() {
		// testing the case where file path for committee is provided instead of
		// directory path
		Assert.assertTrue(new MyParser("dblp_test4.xml", "committee_test1").readData());
	}

}
