/*
 * 
 */
package DBLPTest;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import DBLP.DBLP.AppTest;
import frontend.frontEndTestCase1;
import queryenginetest.QueryEngineTest;

/**
 * The Class AllTests.
 */
@RunWith(Suite.class)
@SuiteClasses({
	frontEndTestCase1.class,
	QueryEngineTest.class,
	AppTest.class
})
public class AllTests {

}
