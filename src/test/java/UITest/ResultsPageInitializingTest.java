package UITest;

import static org.junit.Assert.*;

import java.awt.Component;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;

import org.junit.Test;

import user_interface.ResultsPage;

public class ResultsPageInitializingTest {

	@Test
	public void test() {
		ResultsPage rpage= new ResultsPage();
        rpage.main(null);
        Component[] components=rpage.getContentPane().getComponents();
        
        for (Component c :components)
        {
        	if(c instanceof JTabbedPane)
        	{
        		System.out.println("hello");
        		JTabbedPane pane=(JTabbedPane) c;
        		
        		int totalTabs = pane.getTabCount();
        		for(int i = 0; i < totalTabs; i++)
        		{
        		   Component jpanel = pane.getComponentAt(i);
        		   System.out.println(" inside TabbedPane");
        		  if(jpanel instanceof JPanel){
        			   JPanel jp=(JPanel)jpanel;
        			   
        			   Component[] jpanelcomponents=jp.getComponents();
        			   
        			   for (Component eachpanelComponents :jpanelcomponents)
        		        {
        			if(eachpanelComponents instanceof JTextField)
   	                	{
        				JTextField allbutton=(JTextField)eachpanelComponents;
        				allbutton.setText("mac");
   					  System.out.println("writing text field ");
   	                		
   	                	}
        				   
        				   if(eachpanelComponents instanceof JButton)
        	                	{
        					   JButton allbutton=(JButton)eachpanelComponents;
        					   allbutton.doClick();
        					  System.out.println("button inside button");
        	                		
        	                	}
        		        }
             }
        		   
        		}
               

        }

		
	}
	}

}
