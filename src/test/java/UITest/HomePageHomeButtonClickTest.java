package UITest;

import static org.junit.Assert.*;

import java.awt.Component;

import javax.swing.AbstractButton;
import javax.swing.JLabel;

import org.junit.Test;

import com.mysql.jdbc.Connection;

import user_interface.HomePageSearch;

public class HomePageHomeButtonClickTest {

	@Test
	public void test() {
	 
	   HomePageSearch frame =new HomePageSearch();
		Component[] components=frame.getContentPane().getComponents();
		((AbstractButton) components[0]).doClick();
		JLabel textCheck=(JLabel) components[1];
		assertEquals("BiblioSearch",textCheck.getText());
	    assertNotNull(frame.getResultPageCreated());
		
	}

}
