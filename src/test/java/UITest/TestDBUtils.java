/*
 * 
 */
package UITest;

import static org.junit.Assert.*;

import java.sql.ResultSet;

import org.junit.Ignore;
import org.junit.Test;

import com.mysql.jdbc.Statement;

import frontend.DBUtils;

// TODO: Auto-generated Javadoc
/**
 * The Class TestDBUtils.
 */
public class TestDBUtils {

	/** The rs. */
	ResultSet rs;
	
	/** The st. */
	Statement st;
	
	/**
	 * Test search authorby name 1.
	 */
	
	
	public void testSearchAuthorbyName1() {
		rs = DBUtils.getInstance().searchAuthorByName("sai");
		assertNotEquals(null, rs);
	}
	
	/**
	 * Test search authorby name 2.
	 */
	
	@Test
	public void testSearchAuthorbyName2() {
		rs = DBUtils.getInstance().searchAuthorByName("sai k");
		assertNotEquals(null, rs);
	}
	
	/**
	 * Test search journalby title 1.
	 */
	
	@Test
	public void testSearchJournalbyTitle1() {
		rs = DBUtils.getInstance().searchJournalByTitle("art");
		assertNotEquals(null, rs);
	}
	
	/**
	 * Test search journalby title 2.
	 */
	
	@Test
	public void testSearchJournalbyTitle2() {
		rs = DBUtils.getInstance().searchJournalByTitle("art wo");
		assertNotEquals(null, rs);
	}
	
	/**
	 * Test search conferenceby title 1.
	 */
	
	@Test
	public void testSearchConferencebyTitle1() {
		rs = DBUtils.getInstance().searchConferenceByTitle("art");
		assertNotEquals(null, rs);
	}
	
	/**
	 * Test search conferenceby title 2.
	 */
	
	@Test
	public void testSearchConferencebyTitle2() {
		rs = DBUtils.getInstance().searchConferenceByTitle("art wo");
		assertNotEquals(null, rs);
	}
	
	/**
	 * Testsearch domain by keyword 1.
	 */
	
	@Test
	public void testsearchDomainByKeyword1() {
		rs = DBUtils.getInstance().searchDomainByKeyword("art");
		assertNotEquals(null, rs);
	}
	
	/**
	 * Testsearch domain by keyword 2.
	 */
	
	@Test
	public void testsearchDomainByKeyword2() {
		rs = DBUtils.getInstance().searchDomainByKeyword("art wo");
		assertNotEquals(null, rs);
	}
	
	/**
	 * Test result setto table model.
	 */
	
	@Test
	
	public void testResultSettoTableModel() {
		
		rs = DBUtils.getInstance().searchDomainByKeyword("art wo");
		assertEquals(null, DBUtils.getInstance().resultSettoTableModel(rs));
	}

	/**
	 * Test result setto table model authors.
	 */
	
	@Test
	public void testResultSettoTableModelAuthors() {
		rs = DBUtils.getInstance().searchAuthorByName("sai");
		assertNotEquals(null, DBUtils.getInstance().resultSettoTableModelAuthors(rs,"John"));
	}
	
	/**
	 * Test result setto table model journal.
	 */
	
	@Test
	public void testResultSettoTableModelJournal() {
		rs = DBUtils.getInstance().searchDomainByKeyword("art wo");
		assertNotEquals(null, DBUtils.getInstance().resultSettoTableModelJournal(rs));
	}
}
