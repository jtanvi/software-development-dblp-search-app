package UITest;

import static org.junit.Assert.*;

import java.awt.Component;

import javax.swing.AbstractButton;

import org.junit.Test;

import com.mysql.jdbc.Connection;

import user_interface.HomePageSearch;

public class HomePageInitiComponentsTest {

	@Test
	public void test() {
		    HomePageSearch frame =new HomePageSearch();
			Component[] components=frame.getContentPane().getComponents();
			assertEquals(components.length, 3);
	}

}
