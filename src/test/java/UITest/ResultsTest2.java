package UITest;

import static org.junit.Assert.*;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.InputEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;

import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JTable;

import org.junit.Test;

import user_interface.DataTransferHandler;
import user_interface.ResultsPage;

public class ResultsTest2 {
	
	private ResultsPage page = new ResultsPage();

	@Test
	public void testFavFrame() {
		assertTrue(page.getFavorites() instanceof JFrame);
	}
	
	
	@Test
	public void testAddMotionListener() {
		JTable table = page.fav.getFavoritesTable();
		page.addMotionListener(table);
		
	}
	
	@Test
	public void testGetTable() {
		JTable table = page.fav.getFavoritesTable();
		assertTrue(page.getTable() instanceof JTable);
	}
	
	@Test
	public void testGetTable2() throws AWTException {
		Robot bot = new Robot();
		bot.mouseMove(94,14);
		bot.mousePress(InputEvent.BUTTON1_MASK);
		//add time between press and release or the input event system may 
		//not think it is a click
		//try{Thread.sleep(250);}catch(InterruptedException e){}
		bot.mouseRelease(InputEvent.BUTTON1_MASK);
		//JComponent[] comp = (JComponent[]) page.getComponents();	
		JTable table = page.fav.getFavoritesTable();
		assertTrue(page.getTable() instanceof JTable);
	}

	
	
	

}
