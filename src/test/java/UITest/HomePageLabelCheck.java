package UITest;

import static org.junit.Assert.*;

import java.awt.Component;

import javax.swing.AbstractButton;
import javax.swing.JLabel;

import org.junit.Test;

import user_interface.HomePageSearch;

public class HomePageLabelCheck {

	@Test
	public void test() {
		HomePageSearch frame =new HomePageSearch();
		frame.main(null);
		Component[] components=frame.getContentPane().getComponents();
		JLabel textCheck=(JLabel) components[1];
		assertEquals("BiblioSearch",textCheck.getText());
	  
	}

}
