/*
 * 
 */
package UITest;

import static org.junit.Assert.*;


import java.sql.Connection;
import java.sql.ResultSet;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.mysql.jdbc.Statement;

import user_interface.HomePageSearch;
import frontend.*;

// TODO: Auto-generated Javadoc
/**
 * The Class TestUI.
 */
// This class checks DB Connection
public class TestUI {
	
	/** The conn. */
	Connection conn = null;
	
	/** The instance. */
	private DBUtils instance = null;
	
	/** The st. */
	Statement st = null;
    
    
    /**
     * Before.
     */
    @Before
    public void before() {
    	
    	//HomePageSearch page = new HomePageSearch();
    	
    	instance = DBUtils.getInstance();
    	//instance = instance.closeConnection();
    }
    

    /**
     * After.
     */
    @After
    public void after() {
    	instance = DBUtils.getInstance();
        //HomePageSearch.closeConnection(connection);
    	//HomePageSearch.closeConnection(conn);
    }
    
	
	/**
	 * Test DB connection.
	 */
	@Test
	public void testDBConnection() {
		assertEquals(null, conn);
	}
	
	/*
	@Test
	public void testExecuteBatch() {
		assertEquals(null,st);
	}
	*/
	
	
	
}
