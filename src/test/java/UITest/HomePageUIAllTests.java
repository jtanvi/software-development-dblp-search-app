package UITest;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ HomePageHelpFunctionalityCheck.class, HomePageHomeButtonClickTest.class,
		HomePageInitiComponentsTest.class, HomePageLabelCheck.class })
public class HomePageUIAllTests {
}
