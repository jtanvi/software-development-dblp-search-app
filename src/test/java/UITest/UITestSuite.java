/*
 * 
 */
package UITest;

import static org.junit.Assert.*;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;


/**
 * The Class UITestSuite.
 */
@RunWith(Suite.class)
@SuiteClasses({
	TestUI.class,
	ConnectionTest.class,
	ResultsTest.class,
	FavoritesTest.class,
	DataTransferHandlerTest.class,
	AuthorsDetailsPageTest.class,
	HomePageUIAllTests.class,
	ResultsPageInitializingTest.class,
	ActivityIndicatorTest.class,
	ResultsTest2.class
})


public class UITestSuite {

	

}
