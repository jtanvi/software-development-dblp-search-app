package UITest;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;
import java.io.IOException;
import java.util.ArrayList;

import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import org.junit.Test;

import user_interface.Favorites;

public class FavoritesTest {
	
	private Favorites fav = new Favorites();
	
	@Test
	public void test() {
		// Add columns to table
		String[] columns = {"Column 1","Column 2"};
		DefaultTableModel model = new DefaultTableModel(columns, 0);
		Object[] data = {};		
		
		// Add 2
		JTable fTable = fav.getFavoritesTable();
		fTable.setModel(model);
		int colCount = fTable.getColumnCount();
		ArrayList<String> list2 = new ArrayList<String>();
		for (int i = 0; i < colCount; i++) {
			list2.add("string");
		}
		data = list2.toArray();
		((DefaultTableModel) fTable.getModel()).addRow(data);
		((DefaultTableModel) fTable.getModel()).addRow(data);

		fTable.setRowSelectionInterval(0, 0);
		fav.btnRemoveActionPerformed(fTable);
		fav.btnExportActionPerformed();
	}
	
	@Test
	public void testMain() {
		
		fav.main(null);
	}
	
//	@Test
//	public void testExport() throws IOException {
//	
//		String str = fav.OpenFileChooser();
//		assertNotEquals(str, null);
//	}

}
