package UITest;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;

import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.util.ArrayList;

import javax.activation.ActivationDataFlavor;
import javax.activation.DataHandler;
import javax.swing.JTable;
import javax.swing.TransferHandler;
import javax.swing.TransferHandler.TransferSupport;
import javax.swing.table.DefaultTableModel;

import org.junit.Test;

import user_interface.AuthorsDetailsPage;
import user_interface.DataTransferHandler;
import user_interface.Favorites;
import user_interface.ResultsPage;

public class DataTransferHandlerTest {

	
	private TransferHandler handler = new DataTransferHandler();
	private AuthorsDetailsPage page = new AuthorsDetailsPage();
	private ResultsPage rPage = new ResultsPage();
	private Favorites fav = new Favorites();
	JTable sTable = page.getSimilarAuthorsTable();
	//private TransferSupport supp = new TransferSupport(fav.getFavoritesTable(), null);
	
	@Test
	public void testSourcePage() {
		assertNotEquals(page, null);
	}
	
	@Test
	public void testHandler() {
		assertNotEquals(handler, null);
	}
	
	@Test
	public void testSourceTable() {
		assertNotEquals(sTable, null);
	}
	
	@Test
	public void testGetSources() {
		
		rPage.addMotionListener(sTable);
		assertEquals(sTable.getTransferHandler().getSourceActions(sTable), TransferHandler.COPY_OR_MOVE);
	}
	
	
//	@Test
//	public void testImport() {
//		assertTrue(fav.getFavoritesTable().getTransferHandler().canImport(supp));
//	}
//	
	
	
	
	@Test
	public void testCreateTransferable() {
		String[] columns = {"Column 1","Column 2"};
		DefaultTableModel model = new DefaultTableModel(columns, 0);
		sTable.setModel(model);
		int colCount = sTable.getColumnCount();
		ArrayList list = new ArrayList();
		Object[] data = {};
		for (int i = 0; i < colCount; i++) {
			list.add("column");
		}
		data = list.toArray();
		model.addRow(data);
		model.addRow(data);

		sTable.setRowSelectionInterval(0, 0);
		rPage.addMotionListener(sTable);
		sTable.getTransferHandler().getSourceActions(sTable);
		Transferable t = (Transferable) ((DataTransferHandler) sTable.getTransferHandler()).getTransferableObject(sTable);
		TransferSupport supp = new TransferSupport(fav.getFavoritesTable(), t);
		
		ActivationDataFlavor localObjectFlavor = new ActivationDataFlavor(Object[].class, DataFlavor.javaJVMLocalObjectMimeType, "Array of items");
		//supp.setDataFa
		
		//supp.setDropAction(TransferHandler.COPY);
		assertEquals(fav.getFavoritesTable().getTransferHandler().canImport(supp), false);
		assertEquals(fav.getFavoritesTable().getTransferHandler().importData(supp), false);
	}
	
	@Test
	public void testCanImport() {
		JTable table = fav.getFavoritesTable();
		//assertTrue(table.getTransferHandler());
	}
	
	@Test
	public void testExistInTable() {
		// Add columns to table
		String[] columns = {"Column 1","Column 2"};
		DefaultTableModel model = new DefaultTableModel(columns, 0);
		sTable.setModel(model);
		int colCount = sTable.getColumnCount();
		
		// Add 1
		ArrayList<String> list = new ArrayList<String>();
		Object[] data = {};		
		for (int i = 0; i < colCount; i++) {
			list.add("column");
		}
		data = list.toArray();
		((DefaultTableModel) sTable.getModel()).addRow(data);
		((DefaultTableModel) sTable.getModel()).addRow(data);
		
		// Add 2
		JTable fTable = fav.getFavoritesTable();
		fTable.setModel(model);
		ArrayList<String> list2 = new ArrayList<String>();
		for (int i = 0; i < colCount; i++) {
			list2.add("string");
		}
		((DefaultTableModel) fTable.getModel()).addRow(data);
		data = list2.toArray();
		((DefaultTableModel) fTable.getModel()).addRow(data);
		((DefaultTableModel) fTable.getModel()).addRow(data);
		
		sTable.setRowSelectionInterval(0, 0);
		((DataTransferHandler) sTable.getTransferHandler()).copyData(sTable, fTable); //fav.getFavoritesTable());
		assertNotEquals(sTable.getRowCount(), 2);
	}
	
	
	@Test
	public void testNotExistInTable() {
		// Add columns to table
		String[] columns = {"Column 1","Column 2"};
		DefaultTableModel model = new DefaultTableModel(columns, 0);
		sTable.setModel(model);
		int colCount = sTable.getColumnCount();
		
		// Add 1
		ArrayList<String> list = new ArrayList<String>();
		Object[] data = {};		
		for (int i = 0; i < colCount; i++) {
			list.add("column");
		}
		data = list.toArray();
		((DefaultTableModel) sTable.getModel()).addRow(data);
		((DefaultTableModel) sTable.getModel()).addRow(data);
		
		// Add 2
		JTable fTable = fav.getFavoritesTable();
		fTable.setModel(model);
		
		sTable.setRowSelectionInterval(0, 0);
		((DataTransferHandler) sTable.getTransferHandler()).copyData(sTable, fTable); //fav.getFavoritesTable());
		assertEquals(fTable.getRowCount(), 2);
	}
	
	
	
	


}
