/*
 * 
 */
package queryenginetest;

import static org.junit.Assert.*;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.junit.Test;

import com.mockrunner.mock.jdbc.MockResultSet;

import frontend.DBUtils;
import junit.framework.Assert;
import queryengine.QueryEngine;

// TODO: Auto-generated Javadoc
/**
 * The Class QueryEngineTest.
 */
public class QueryEngineTest {

	/**
	 * Test 1 qe.
	 */
	 @Test
	 public void test1(){
		 MockResultSet rs = new MockResultSet("myMock");
		 rs.addColumn("year", new String[]{"2016"});
		 QueryEngine quer= new QueryEngine();
		 String [] yer = {"2016"};
		 String [] res = quer.getYears(rs);
		 assertEquals(yer,res);
	 }

	
	
}
