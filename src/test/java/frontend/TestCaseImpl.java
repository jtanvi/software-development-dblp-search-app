/*
 * 
 */
package frontend;
import frontend.DBUtils;
// TODO: Auto-generated Javadoc

/**
 * The Class TestCaseImpl.
 */
public class TestCaseImpl {
	
	/** The inp. */
	InProceedings inp = new InProceedings();
	
	/** The inp check flag. */
	boolean inpCheckFlag = false;
	
	/** The inp null check flag. */
	boolean inpNullCheckFlag = false;
	
	

	
	/** The www. */
	Www www = new Www();
	
	/** The www check flag. */
	boolean wwwCheckFlag = false;
	
	/**
	 * Inp null check.
	 *
	 * @return true, if successful
	 */
	public boolean inpNullCheck(){
		return inpNullCheckFlag;
	}
	
	/**
	 * Inp check.
	 *
	 * @return true, if successful
	 */
	public boolean inpCheck(){
		return inpCheckFlag;
	}
	
	/**
	 * Run test cases.
	 *
	 * @return true, if successful
	 */
	public boolean runTestCases(){
		try{
		DBUtils.getInstance().addInproceedingsToDB(inp);
		DBUtils.getInstance().addInproceedingsToDB(null);
		DBUtils.getInstance().addWwwToDb(www);
		DBUtils.getInstance().addWwwToDb(null);
		//DBUtils.getInstance().Gusername ="amogh";
		DBUtils.getInstance().addWwwToDb(www);
		DBUtils.getInstance().executeBatch();
		return true;}
		catch(Exception e){
			return false;
		}
	}

}
