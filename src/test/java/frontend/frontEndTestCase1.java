/***
 * Test case for correct inputs 
 * 
 */

package frontend;

import static org.junit.Assert.*;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Properties;

import javax.swing.table.TableModel;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;

import com.mockrunner.mock.jdbc.MockResultSet;

import frontend.MyParser;
import frontend.Handler;
import frontend.DBUtils;

// TODO: Auto-generated Javadoc
/**
 * The Class frontEndTestCase1.
 */
public class frontEndTestCase1 {

	// test case for correct inputs

	/**
	 * Test 20.
	 *
	 * @throws Exception
	 *             the exception
	 */
	@Test
	public void test20() throws Exception {
		// testing the case where file path for committee is provided instead of
		// directory path

		Assert.assertTrue(new TestCaseImpl().runTestCases());

	}

	/**
	 * Test 1.
	 */
	@Test
	public void test01() {
		Properties prop = new Properties();
		try {
			OutputStream output = new FileOutputStream("config.properties");

			prop.setProperty("dbusername", commons.Constants.TestUsername);
			prop.setProperty("dbpassword", commons.Constants.TestPassword);
			prop.setProperty("jdbcString", "jdbc:mysql://localhost:3306/DBLP_SEARCH");
			prop.setProperty("authorFile", "author_test1.csv");
			prop.setProperty("driver", "com.mysql.jdbc.Driver");
			prop.setProperty("jdbcString", "jdbc:mysql://localhost:3306/DBLP_SEARCH");
			prop.setProperty("committeeLocation", "committee_test1");
			prop.setProperty("dblpLocation", "dblp_test1.xml");
			prop.store(output, null);

			MyParser m = new MyParser("", "");
			Assert.assertEquals(true, m.readData());

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * Test 2.
	 */
	@Test
	public void test02() {
		Properties prop = new Properties();
		try {
			OutputStream output = new FileOutputStream("config.properties");

			prop.setProperty("dbusername", commons.Constants.TestUsername);
			prop.setProperty("dbpassword", commons.Constants.TestPassword);
			prop.setProperty("jdbcString", "jdbc:mysql://localhost:3306/DBLP_SEARCH");
			prop.setProperty("authorFile", "author_test1.csv");
			prop.setProperty("driver", "com.mysql.jdbc.Driver");
			prop.setProperty("jdbcString", "jdbc:mysql://localhost:3306/DBLP_SEARCH");
			prop.setProperty("committeeLocation", "committee_test1");
			prop.setProperty("dblpLocation", "dblp_test2.xml");
			prop.store(output, null);

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		MyParser m = new MyParser("", "");
		Assert.assertEquals(false, m.readData());
	}

	/**
	 * Test 3.
	 */
	@Test
	public void test03() {

		Properties prop = new Properties();
		try {
			OutputStream output = new FileOutputStream("config.properties");
		
		prop.setProperty("dbusername", commons.Constants.TestUsername);
		prop.setProperty("dbpassword", commons.Constants.TestPassword);
		prop.setProperty("jdbcString", "jdbc:mysql://localhost:3306/DBLP_SEARCH");
		prop.setProperty("authorFile", "author_test1.csv");
		prop.setProperty("driver", "com.mysql.jdbc.Driver");
		prop.setProperty("jdbcString", "jdbc:mysql://localhost:3306/DBLP_SEARCH");
		prop.setProperty("committeeLocation", "committee_test11");
		prop.setProperty("dblpLocation", "dblp_test1.xml");
		prop.store(output, null);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		MyParser m = new MyParser("", "");
		Assert.assertEquals(true, MyParser.readData());
	}

	/**
	 * Test 4.
	 */
	@Test
	public void test04() {
		Properties prop = new Properties();
		try {
			OutputStream output = new FileOutputStream("config.properties");
		
		prop.setProperty("dbusername", commons.Constants.TestUsername);
		prop.setProperty("dbpassword", commons.Constants.TestPassword);
		prop.setProperty("jdbcString", "jdbc:mysql://localhost:3306/DBLP_SEARCH");
		prop.setProperty("authorFile", "author_test1.csv");
		prop.setProperty("driver", "com.mysql.jdbc.Driver");
		prop.setProperty("jdbcString", "jdbc:mysql://localhost:3306/DBLP_SEARCH");
		prop.setProperty("committeeLocation", "def");
		prop.setProperty("dblpLocation", "abc.xml");
		prop.store(output, null);
		
		
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		MyParser m = new MyParser("", "");
		// wrong paths for both dblp and committee
		Assert.assertEquals(false, m.readData());
	}

	/**
	 * Test 5.
	 */
	@Test
	public void test05() {
		Properties prop = new Properties();
		try {
			OutputStream output = new FileOutputStream("config.properties");
		
		prop.setProperty("dbusername", commons.Constants.TestUsername);
		prop.setProperty("dbpassword", commons.Constants.TestPassword);
		prop.setProperty("jdbcString", "jdbc:mysql://localhost:3306/DBLP_SEARCH");
		prop.setProperty("authorFile", "author_test1.csv");
		prop.setProperty("driver", "com.mysql.jdbc.Driver");
		prop.setProperty("jdbcString", "jdbc:mysql://localhost:3306/DBLP_SEARCH");
		prop.setProperty("committeeLocation", "committee_test1/ase2001-pc.txt");
		prop.setProperty("dblpLocation", "dblp_test1.xml");
		prop.store(output, null);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		MyParser m = new MyParser("","");
		// testing the case where file path for committee is provided instead of
		// directory path
		Assert.assertTrue(m.readData());
	}

	/**
	 * Test 6.
	 */
	@Test
	public void test6() {
		
		
		Properties prop = new Properties();
		try {
			OutputStream output = new FileOutputStream("config.properties");
		
		prop.setProperty("dbusername", commons.Constants.TestUsername);
		prop.setProperty("dbpassword", commons.Constants.TestPassword);
		prop.setProperty("jdbcString", "jdbc:mysql://localhost:3306/DBLP_SEARCH");
		prop.setProperty("authorFile", "author_test1.csv");
		prop.setProperty("driver", "com.mysql.jdbc.Driver");
		prop.setProperty("jdbcString", "jdbc:mysql://localhost:3306/DBLP_SEARCH");
		prop.setProperty("committeeLocation", "committees_test4");
		prop.setProperty("dblpLocation", "dblp_test1.xml");
		prop.store(output, null);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		MyParser m = new MyParser("","");
		
				
		
		// testing the case where file path for committee is provided instead of
		// directory path
		Assert.assertTrue(m.readData());
	}

	/**
	 * Test 7.
	 */
	@Test
	public void test7() {
		Properties prop = new Properties();
		try {
			OutputStream output = new FileOutputStream("config.properties");
		
		prop.setProperty("dbusername", commons.Constants.TestUsername);
		prop.setProperty("dbpassword", commons.Constants.TestPassword);
		prop.setProperty("jdbcString", "jdbc:mysql://localhost:3306/DBLP_SEARCH");
		prop.setProperty("authorFile", "author_test1.csv");
		prop.setProperty("driver", "com.mysql.jdbc.Driver");
		prop.setProperty("jdbcString", "jdbc:mysql://localhost:3306/DBLP_SEARCH");
		prop.setProperty("committeeLocation", "committees_test1");
		prop.setProperty("dblpLocation", "dblp_test3.xml");
		prop.store(output, null);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		MyParser m = new MyParser("","");
		// testing the case where file path for committee is provided instead of
		// directory path
		Assert.assertFalse(m.readData());
	}

	/**
	 * Test 8.
	 */
	@Test
	public void test8() {
		Assert.assertTrue(new Www().getYear() == 0);
	}

	/**
	 * Test 9.
	 */
	@Test
	public void test9() {
		Assert.assertTrue(new InProceedings().getYear() == 0);
	}

	/**
	 * Test 10.
	 */
	@Test
	public void test10() {
		Assert.assertTrue(new PublicationFactory().getPublication("mastersthesis", null, null) == null);
	}

	/**
	 * Test 11.
	 */
	@Test
	public void test11() {
		Assert.assertTrue(new PublicationFactory().getPublication(null, null, null) == null);
	}

	/**
	 * Test 12.
	 */
	@Test
	public void test12() {
		// Assert.assertFalse(new
		// PublicationFactory().getPublication("article",null,null)==null);
	}

	/**
	 * Test 13.
	 */
	@Test
	public void test13() {
		Assert.assertTrue(new Article().getYear() == 0);
	}

	/**
	 * Test 14.
	 */
	@Test
	public void test14() {
		Assert.assertTrue(new MyParser("dblp_test1.xml", "committees").readData());
	}

	/**
	 * Test 15.
	 */
	@Test
	public void test15() {

		Assert.assertTrue(new MyParser("dblp_test4.xml", "committee_test1").readData());
	}

	/**
	 * Test 16.
	 */
	@Test
	public void test16() {

		Assert.assertTrue(new MyParser("dblp_test1.xml", "ase2001-pc.txt").readData());
	}

	/**
	 * Test 17.
	 *
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	@Test(expected = IOException.class)
	public void test17() throws IOException {
		// testing the case where file path for committee is provided instead of
		// directory path

		new CommitteeParser().processAndLoad(new BufferedReader(new FileReader("a.txt")), "ase2001-pc.txt");

	}

	/**
	 * Test 18.
	 *
	 * @throws Exception
	 *             the exception
	 */
	@Test
	public void test18() throws java.lang.Exception {
		// testing the case where file path for committee is provided instead of
		// directory path

		new CommitteeParser().parseAndLoad(new File("ase2000002.txt"));

	}

	/**
	 * Test 19.
	 *
	 * @throws Exception
	 *             the exception
	 */
	@Test
	public void test19() throws Exception {
		// testing the case where file path for committee is provided instead of
		// directory path

		Assert.assertTrue(new MyParser("dblp_test1.xml", "committees_test4").readData());

	}

	@Test
	public void test21() {
		boolean res;
		commons.Constants c = new commons.Constants();
		res = "author".equalsIgnoreCase(c.AUTHOR);
		Assert.assertTrue(res);

	}

	@Test
	public void test22() {
		Properties prop = new Properties();
		try {
			OutputStream output = new FileOutputStream("config.properties");
			prop.setProperty("dbusername", commons.Constants.TestUsername);
			prop.setProperty("dbpassword", commons.Constants.TestPassword);
			prop.setProperty("jdbcString", "jdbc:mysql://localhost:3306/DBLP_SEARCH");
			prop.setProperty("authorFile", "author_test1.csv");
			prop.store(output, null);

			MyParser p = new MyParser("", "");
			p.readAuthors();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	
	@Test//(expected = IOException.class)
	public void test23() {
		Properties prop = new Properties();
		try {
			OutputStream output = new FileOutputStream("config1.properties");
			prop.setProperty("dbusername", commons.Constants.TestUsername);
			prop.setProperty("dbpassword", commons.Constants.TestPassword);
			prop.setProperty("jdbcString", "jdbc:mysql://localhost:3306/DBLP_SEARCH");
			prop.setProperty("authorFile", "author_test1.csv");
			prop.store(output, null);

			MyParser p = new MyParser("", "");
			p.readAuthors();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	
	
	@Test
	public void test24() {
		Properties prop = new Properties();
		try {
			OutputStream output = new FileOutputStream("config1.properties");
			prop.setProperty("dbusername", commons.Constants.TestUsername);
			prop.setProperty("dbpassword", commons.Constants.TestPassword);
			prop.setProperty("jdbcString", "jdbc:mysql://localhost:3306/DBLP_SEARCH");
			prop.setProperty("authorFile", "author_test1.csv");
			prop.store(output, null);
			
			HashMap<String,Integer> hm = new HashMap<String,Integer>();

			DBUtils.getInstance().setAuthorPageNames(hm);
			Assert.assertTrue(hm.equals(DBUtils.getInstance().getAuthorPageNames()));

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	
	@Test
	public void test25() {
		Properties prop = new Properties();
		try {
			OutputStream output = new FileOutputStream("config1.properties");
			prop.setProperty("dbusername", commons.Constants.TestUsername);
			prop.setProperty("dbpassword", commons.Constants.TestPassword);
			prop.setProperty("jdbcString", "jdbc:mysql://localhost:3306/DBLP_SEARCH");
			prop.setProperty("authorFile", "author_test1.csv");
			prop.store(output, null);
			
			ArrayList<String> hm = new ArrayList<String>();

			DBUtils.getInstance().setAuthorNames(hm);
			Assert.assertTrue(hm.equals(DBUtils.getInstance().getAuthorNames()));

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	
	
	@Test
	public void test26() {
		Properties prop = new Properties();
		try {
			OutputStream output = new FileOutputStream("config1.properties");
			prop.setProperty("dbusername", commons.Constants.TestUsername);
			prop.setProperty("dbpassword", commons.Constants.TestPassword);
			prop.setProperty("jdbcString", "jdbc:mysql://localhost:3306/DBLP_SEARCH");
			prop.setProperty("authorFile", "author_test1.csv");
			prop.store(output, null);
			
			ArrayList<String> hm = new ArrayList<String>();

			DBUtils.getInstance().executeBatch();
			Assert.assertTrue(true);

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	
	@Test
	public void test27() {
		Properties prop = new Properties();
		try {
			OutputStream output = new FileOutputStream("config1.properties");
			prop.setProperty("dbusername", commons.Constants.TestUsername);
			prop.setProperty("dbpassword", "root");
			prop.setProperty("jdbcString", "jdbc:mysql://localhost:3306/DBLP_SEARCH");
			prop.setProperty("authorFile", "author_test1.csv");
			prop.store(output, null);
			
			ArrayList<String> hm = new ArrayList<String>();
			
			DBUtils.getInstance().setStatement(null);
			DBUtils.getInstance().executeBatch();
			Assert.assertTrue(true);

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	
	@Test
	public void test28() {
		Properties prop = new Properties();
		try {
			OutputStream output = new FileOutputStream("config1.properties");
			prop.setProperty("dbusername", commons.Constants.TestUsername);
			prop.setProperty("dbpassword", commons.Constants.TestPassword);
			prop.setProperty("jdbcString", "jdbc:mysql://localhost:3306/DBLP_SEARCH");
			prop.setProperty("authorFile", "author_test1.csv");
			prop.store(output, null);
			
			ArrayList<String> hm = new ArrayList<String>();
          
			DBUtils.getInstance().setStatement(null);
			DBUtils.getInstance().updateYearInCommittee();
			
			Assert.assertTrue(true);

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	
	@Test
	public void test29() {
		Properties prop = new Properties();
		try {
			OutputStream output = new FileOutputStream("config1.properties");
			prop.setProperty("dbusername", commons.Constants.TestUsername);
			prop.setProperty("dbpassword", commons.Constants.TestPassword);
			prop.setProperty("jdbcString", "jdbc:mysql://localhost:3306/DBLP_SEARCH");
			prop.setProperty("authorFile", "author_test1.csv");
			prop.store(output, null);
			
			InProceedings inp = new InProceedings();
			DBUtils.getInstance().addInproceedingsToDB(inp);
			
			Assert.assertTrue(true);

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	
	@Test
	public void test30() {
		Properties prop = new Properties();
		try {
			OutputStream output = new FileOutputStream("config1.properties");
			prop.setProperty("dbusername", commons.Constants.TestUsername);
			prop.setProperty("dbpassword", commons.Constants.TestPassword);
			prop.setProperty("jdbcString", "jdbc:mysql://localhost:3306/DBLP_SEARCH");
			prop.setProperty("authorFile", "author_test1.csv");
			prop.store(output, null);
			
			CommitteeParser c = new CommitteeParser();
			String x = c.getConfChairFlag("C");
			Assert.assertTrue(x.equals("T"));

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	
	
	@Test
	public void test31() {
		Properties prop = new Properties();
		try {
			OutputStream output = new FileOutputStream("config1.properties");
			prop.setProperty("dbusername", commons.Constants.TestUsername);
			prop.setProperty("dbpassword", commons.Constants.TestPassword);
			prop.setProperty("jdbcString", "jdbc:mysql://localhost:3306/DBLP_SEARCH");
			prop.setProperty("authorFile", "author_test1.csv");
			prop.store(output, null);
			
			CommitteeParser c = new CommitteeParser();
			String x = c.getExternalReviewFlag("E");
			Assert.assertTrue(x.equals("T"));

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	
	@Test
	public void test32() {
		Properties prop = new Properties();
		try {
			OutputStream output = new FileOutputStream("config1.properties");
			prop.setProperty("dbusername", commons.Constants.TestUsername);
			prop.setProperty("dbpassword", commons.Constants.TestPassword);
			prop.setProperty("jdbcString", "jdbc:mysql://localhost:3306/DBLP_SEARCH");
			prop.setProperty("authorFile", "author_test1.csv");
			prop.store(output, null);
			
			CommitteeParser c = new CommitteeParser();
			c.setGlobalCount(10000055);
			c.processAndLoad(new BufferedReader(new FileReader("committee_test1/ase2001-pc.txt")), "committee_test1/ase2001-pc.txt");

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	
	@Test(expected = NullPointerException.class)
	public void test33() {
		Properties prop = new Properties();
		try {
			OutputStream output = new FileOutputStream("config1.properties");
			prop.setProperty("dbusername", commons.Constants.TestUsername);
			prop.setProperty("dbpassword", commons.Constants.TestPassword);
			prop.setProperty("jdbcString", "jdbc:mysql://localhost:3306/DBLP_SEARCH");
			prop.setProperty("authorFile", "author_test1.csv");
			prop.store(output, null);
			
			
		
			Handler h = new Handler();
			h.setGlobalCount(10000002);
			h.startElement("www", "www", "www", null);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SAXException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}


	@Test
	public void test34() {
		Properties prop = new Properties();
		try {
			OutputStream output = new FileOutputStream("config1.properties");
			prop.setProperty("dbusername", commons.Constants.TestUsername);
			prop.setProperty("dbpassword", commons.Constants.TestPassword);
			prop.setProperty("jdbcString", "jdbc:mysql://localhost:3306/DBLP_SEARCH");
			prop.setProperty("authorFile", "author_test3.csv");
			prop.store(output, null);

			MyParser p = new MyParser("", "");
			p.readAuthors();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	
	@Test
	public void test35() {
		Properties prop = new Properties();
		try {
			OutputStream output = new FileOutputStream("config1.properties");
			prop.setProperty("dbusername", commons.Constants.TestUsername);
			prop.setProperty("dbpassword", commons.Constants.TestPassword);
			prop.setProperty("jdbcString", "jdbc:mysql://localhost:3306/DBLP_SEARCH");
			prop.setProperty("authorFile", "author_test2.csv");
			prop.store(output, null);

			MyParser p = new MyParser("", "");
			p.readAuthors();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	
	@Test
	public void test36() {
		Properties prop = new Properties();
		try {
			OutputStream output = new FileOutputStream("config1.properties");
			prop.setProperty("dbusername", commons.Constants.TestUsername);
			prop.setProperty("dbpassword", commons.Constants.TestPassword);
			prop.setProperty("jdbcString", "jdbc:mysql://localhost:3306/DBLP_SEARCH");
			prop.setProperty("authorFile", "author_test4.csv");
			prop.store(output, null);

			MyParser p = new MyParser("", "");
			p.readAuthors();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	
	@Test
	public void test40() {
		Properties prop = new Properties();
		try {
			ResultSet rs= DBUtils.getInstance().searchAuthorByName("Frank Tip");
			TableModel t = DBUtils.getInstance().resultSettoTableModelAuthors(rs,"Frank Tip");
			assertEquals(t.getRowCount(),1);

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	
	
	

	@Test
	public void test37() {
		Properties prop = new Properties();
		try {
			OutputStream output = new FileOutputStream("config1.properties");
			prop.setProperty("dbusername", commons.Constants.TestUsername);
			prop.setProperty("dbpassword", commons.Constants.TestPassword);
			prop.setProperty("jdbcString", "jdbc:mysql://localhost:3306/DBLP_SEARCH");
			prop.setProperty("authorFile", "author_test4.csv");
			prop.store(output, null);

			DBUtils.getInstance().searchConferenceByTitle("OOPSLA");
			

		} catch (IOException e) {
			// TODO Auto-generated catch block
			Assert.fail("Error");
			e.printStackTrace();
		}

	}
	
	@Test
	public void test38() {
		Properties prop = new Properties();
		try {
			OutputStream output = new FileOutputStream("config1.properties");
			prop.setProperty("dbusername", commons.Constants.TestUsername);
			prop.setProperty("dbpassword", commons.Constants.TestPassword);
			prop.setProperty("jdbcString", "jdbc:mysql://localhost:3306/DBLP_SEARCH");
			prop.setProperty("authorFile", "author_test4.csv");
			prop.store(output, null);

			ResultSet rs = DBUtils.getInstance().searchJournalByTitle("Machine");
			TableModel t = DBUtils.getInstance().resultSettoTableModel(rs);
			TableModel t1 = DBUtils.getInstance().resultSettoTableModelJournal(rs);
			

		} catch (IOException e) {
			// TODO Auto-generated catch block
			Assert.fail("Error");
			e.printStackTrace();
		}

	}
	
	@Test
	public void test39() {
		Properties prop = new Properties();
		try {
			OutputStream output = new FileOutputStream("config1.properties");
			prop.setProperty("dbusername", commons.Constants.TestUsername);
			prop.setProperty("dbpassword", commons.Constants.TestPassword);
			prop.setProperty("jdbcString", "jdbc:mysql://localhost:3306/DBLP_SEARCH");
			prop.setProperty("authorFile", "author_test4.csv");
			prop.store(output, null);

			ResultSet rs = DBUtils.getInstance().searchDomainByKeyword("Machine");
			ResultSet rs1 = DBUtils.getInstance().searchDomainByKeyword("Machine,Computer");
			TableModel t = DBUtils.getInstance().resultSettoTableModel(rs);
			TableModel t2 = DBUtils.getInstance().resultSettoTableModelJournal(rs);
			TableModel t3 = DBUtils.getInstance().resultSettoTableModelJournal(rs1);
			

		} catch (IOException e) {
			// TODO Auto-generated catch block
			Assert.fail("Error");
			e.printStackTrace();
		}

	}
	
	@Test
	public void test41() {
		Properties prop = new Properties();
		try {
			ResultSet rs= DBUtils.getInstance().searchAuthorByName("Frank Tip");
			ResultSet rs1= DBUtils.getInstance().searchAuthorByName("Frank Tip,Mike");
			TableModel t = DBUtils.getInstance().resultSettoTableModelAuthors(rs,"Frank Tip");
			assertEquals(t.getRowCount(),1);

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	
	@Test
	public void test42() {
		Properties prop = new Properties();
		try {
			ResultSet rs= DBUtils.getInstance().searchAuthorByName("Frank Tip,Frank Tip");
			TableModel t = DBUtils.getInstance().resultSettoTableModelAuthors(rs,"Frank Tip");
			TableModel t1 = DBUtils.getInstance().resultSettoTableModelAuthors(rs,"Frank Tip,Mike");
			HashMap<String,Integer> hm = new HashMap<String,Integer>();
			
			DBUtils.getInstance().setAuthorConfNames(hm);
			hm = DBUtils.getInstance().getAuthorConfNames();
			DBUtils.getInstance().closeConnection();

		} catch (Exception e) {
			Assert.fail("Error"+ e);
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	@Test
	public void test43() {
		Properties prop = new Properties();
		try {
			ResultSet rs= DBUtils.getInstance().searchAuthorByDetailsInArticle("Frank Tip,Frank Tip");
			ResultSet rs1= DBUtils.getInstance().searchAuthorByDetailsInArticle(null);
			ResultSet rs4= DBUtils.getInstance().searchAuthorByDetailsInArticle("Frank Tip");
			ResultSet rs2= DBUtils.getInstance().searchAuthorByDetails("Frank Tip");
			ResultSet rs3= DBUtils.getInstance().searchAuthorByDetails("Frank Tip,Frank Tip");
			TableModel t = DBUtils.getInstance().resultSettoTableModelAuthors(rs,"Frank Tip");
			TableModel t2 = DBUtils.getInstance().resultSettoTableModelAuthorDetails(rs);
			TableModel t3 = DBUtils.getInstance().resultSettoTableModelSimilarAuthorDetails(rs2);
			TableModel t4 = DBUtils.getInstance().resultSettoTableModelSimilarAuthorDetails(null);
			
			

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	
	@Test
	public void test44() {
		Properties prop = new Properties();
		try {
		
			ResultSet rs2= DBUtils.getInstance().searchAuthorByDetails("Frank Tip");
			ResultSet rs3= DBUtils.getInstance().searchAuthorByDetails("Frank Tip,Frank Tip");
			
			TableModel t2 = DBUtils.getInstance().resultSettoTableModelAuthorDetails(rs2);
			

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	
	@Test
	public void test45() {
		Properties prop = new Properties();
		try {
			ResultSet rs2= DBUtils.getInstance().searchJournalByTitle("Computer");
			TableModel t2 = DBUtils.getInstance().resultSettoTableModelJournal(rs2);
			
			ResultSet rs3= DBUtils.getInstance().searchJournalByTitle("Computer,Machine");
			TableModel t3 = DBUtils.getInstance().resultSettoTableModelJournal(rs3);
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	
	@Test
	public void test46() {
		Properties prop = new Properties();
		try {
			DBUtils.getInstance().addQuery("select c");
			DBUtils.getInstance().addQuery("select d");
			DBUtils.getInstance().executeBatch();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	
	@Test
	public void test47() {
		Properties prop = new Properties();
		try {
			
			ResultSet rs = DBUtils.getInstance().searchAuthorByDetails(null);
		    
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	
	@Test
	public void test50() {
		Properties prop = new Properties();
		try {
			OutputStream output = new FileOutputStream("config1.properties");
			prop.setProperty("dbusername", commons.Constants.TestUsername);
			prop.setProperty("dbpassword", commons.Constants.TestPassword);
			prop.setProperty("jdbcString", "jdbc:mysql://localhost:3306/DBLP_SEARCH");
			prop.setProperty("authorFile", "author_test4.csv");
			prop.store(output, null);

			ResultSet rs = DBUtils.getInstance().searchSimiliarAuthorByDomain("Machine");
			ResultSet rs1 = DBUtils.getInstance().searchSimiliarAuthorByDomain("Frank Tip");
			MockResultSet rsMock = new MockResultSet("mockRs");
			rsMock.addColumn("author",new String[] {"frank,Micheal","george"});
			TableModel t = DBUtils.getInstance().resultSettoTableModel(rsMock);
			TableModel t2 = DBUtils.getInstance().resultSettoTableModelSimilarAuthorDetails(rsMock);
			DBUtils.getInstance().setWarningMsg("MEssages");
			DBUtils.getInstance().setHelpMsg("MEssages");

		} catch (IOException e) {
			// TODO Auto-generated catch block
			Assert.fail("Error");
			e.printStackTrace();
		}

	}





}
