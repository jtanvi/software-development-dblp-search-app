/*
 * 
 */
package frontend;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import junit.framework.Assert;

/**
 * The Class FrontEndTestSuite.
 */
@RunWith(Suite.class)
@SuiteClasses({ frontEndTestCase1.class })
public class FrontEndTestSuite {

}
